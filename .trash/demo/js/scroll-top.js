(function($) {
           var doAnimations = function() {
             var offset = $(window).scrollTop() + $(window).height(),
                 $animatables = $('.animatable');
         		$animatables.each(function(i) {
                var $animatable = $(this);
         			if (($animatable.offset().top + $animatable.height() + 50) < offset) {
                 if (!$animatable.hasClass('animate-in')) {
                   $animatable.removeClass('animate-out').css('top', $animatable.css('top')).addClass('animate-in');
                 }
         			}
               else if (($animatable.offset().top + $animatable.height() + 50) > offset) {
                 if ($animatable.hasClass('animate-in')) {
                   $animatable.removeClass('animate-in').css('top', $animatable.css('top')).addClass('animate-out');
                 }
               }
             });
         	};
         	$(window).on('scroll', doAnimations);
           $(window).trigger('scroll');
         });	
         	jQuery(window).scroll(function(){
             if(jQuery(window).scrollTop()<50){
                 jQuery('#rocketmeluncur').slideUp(500);
             }else{
                 jQuery('#rocketmeluncur').slideDown(500);
             }
             var ftrocketmeluncur = jQuery("#ft")[0] ? jQuery("#ft")[0] : jQuery(document.body)[0];
             var scrolltoprocketmeluncur = $('rocketmeluncur');
         var viewPortHeightrocketmeluncur = parseInt(document.documentElement.clientHeight);
         var scrollHeightrocketmeluncur = parseInt(document.body.getBoundingClientRect().top);
         var basewrocketmeluncur = parseInt(ftrocketmeluncur.clientWidth);
         var swrocketmeluncur = scrolltoprocketmeluncur.clientWidth;
         if (basewrocketmeluncur < 1000) {
         var leftrocketmeluncur = parseInt(fetchOffset(ftrocketmeluncur)['left']);
         leftrocketmeluncur = leftrocketmeluncur < swrocketmeluncur ? leftrocketmeluncur * 2 - swrocketmeluncur : leftrocketmeluncur;
         scrolltoprocketmeluncur.style.left = ( basewrocketmeluncur + leftrocketmeluncur ) + 'px';
         } 
         })
         
         jQuery('#rocketmeluncur').click(function(){
             jQuery("html, body").animate({ scrollTop: '0px',display:'none'},{
                     duration: 600,  
                     easing: 'linear'
                 });
             
             var self = this;
             this.className += ' '+"launchrocket";
             setTimeout(function(){
               self.className = 'showrocket';
             },800)
         });