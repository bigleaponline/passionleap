<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
    }
    public function index()
    {
        $this->load->view('admin/login');
    }
    public function login()
    {

        $login=$this->admin_model->login();
        if($login)
        {
            $this->session->set_userdata('cand_data',$login);
            redirect('dashboard');
        }
        else
        {
            $this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
            redirect('login');
        }
    }
    public function dashboard()
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');

        $data['candidate_count'] = count($this->admin_model->get_candidate_list());
        $this->load->view('admin/admin-header', $data);
        $this->load->view('admin/admin-navbar', $data);
        $this->load->view('admin/index', $data);
    }
    public function logout()
    {
        session_destroy();

        $this->load->view('admin/login');

    }
    public function get_candidate_list()
    {
        $data['candidate_list'] = $this->admin_model->get_candidate_list();
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/candidate_list',$data);
       
    }
    public function view_candidate_details($cand_id)
    {
        $data['cand_id'] = $cand_id;
        $data['candidate_details'] = $this->admin_model->get_candidate_details($cand_id);
        $data['candidate_education'] = $this->admin_model->get_candidate_education($cand_id);
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/candidate_view',$data);
    }
    public function delete_candidate_details($cand_id)
    {
        $this->admin_model->delete_candidate_details($cand_id);
        redirect('candidate_list');
    }
    public function approve_candidate($cand_id)
    {
        $this->admin_model->approve_candidate($cand_id);
        redirect('candidate_list');
    }
    public function question_entry() 
    {

        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('admin/login');
        } 
        else 
        {
            $this->form_validation->set_rules('topic_id', 'Topic Id', 'required');  
            if ($this->form_validation->run() === FALSE)
            {

            } 
            else 
            {

                $this->admin_model->save_question_entry();
            }
            // $data['topics_list'] = $this->admin_model->get_topic_list();
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/question_entry',$data);

        }
    }
    public function question_list() 
    {  
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['question_list'] = $this->admin_model->get_question_master_list();
        if ($this->session->userdata('username') == '') 
        {
            redirect('admin/login');
        } 
        else 
        {
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/question_list', $data);
        }
    }
    public function question_entry_edit($quest_id)
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['id'] = $quest_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('admin/login');
        } 
        else 
        {
            $this->form_validation->set_rules('weightage', 'weightage', 'required');  
            if ($this->form_validation->run() === FALSE)
            {
                $data['question_data'] = $this->admin_model->get_question_detail($quest_id);
        
            } 
            else 
            {
                $question_type = $this->input->post('question_type');
                $this->admin_model->update_question_entry($quest_id,$question_type);
                redirect('question_list');
            }
            $data['question_data'] = $this->admin_model->get_question_detail($quest_id);
            $data['general_question_data'] = $this->admin_model->get_general_question_detail($quest_id);
            $data['passage_question_data'] = $this->admin_model->get_passage_question_detail($quest_id);
            $data['quest_id'] = $quest_id;
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/question_entry_edit', $data);
        }
    }
    public function delete_question_entry($quest_id) 
    {
        $data['id'] = $quest_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('admin/login');
        }
        else 
        {
            $this->admin_model->delete_question_entry($quest_id);
            redirect('question_list');
        }
    }
}
?>
