<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Candidate Details</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Candidate Details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
        <?php 
        foreach($candidate_details as $candidate_details)
        {

        }

        ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <form class="form-horizontal"> -->
                                <span style="color:red;"><?php echo form_error('qualification');?></span>
                                <div class="card-body">
                                     <div  class="row"> 

        <div class="col-md-12">
        <h4><b>Other Details</b></h4>
        </div>
        <br><br><br>

        <div class="col-md-2">
        <label>Name</label>
        </div>
        <div class="col-md-4">
        <input class="form-control" type="text" name="name" id="name"  value="<?php echo $candidate_details['name'];?>" readonly="readonly"/>
        <span class="err"><?php echo form_error('name'); ?></span>
        </div>
       

    
        <div class="col-md-2">
            <label> Highest Qualification</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['qualfcn_catgry'];?>" readonly="readonly"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Whatsapp Number</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['contactnumber'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>Second Number</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['contactnumber'];?>" readonly="readonly"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Email</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['email'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>DOB</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['dob'];?>" readonly="readonly"/>
        </div>

    </div><br>

    <div class="row">
        <div class="col-md-2">
            <label>Address1</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['address_line1'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>Address2</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['address_line2'];?>" readonly="readonly"/>
        </div>

    </div><br>
    <div class="row">
        <div class="col-md-2">
            <label>city</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['city'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>zip_code</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['zip_code'];?>" readonly="readonly"/>
        </div>

    </div><br>
    <div class="row">
        <div class="col-md-2">
            <label>state</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['state'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>country</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['country'];?>" readonly="readonly"/>
        </div>

    </div><br>

    <div class="row">
        <div class="col-md-2">
            <label>Father's Name</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['father_name'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>Father's Occupation</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['father_occupation'];?>" readonly="readonly"/>
        </div>

    </div><br>

    <div class="row">
        <div class="col-md-2">
            <label>Mother's Name</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['mother_name'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>Mother's Occupation</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_details['mother_occupation'];?>" readonly="readonly"/>
        </div>

    </div><br>

    <div class="row">
                    <div class="col-12">


                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id= "table-id">
                                        <thead class="my_head">
                                            <tr>
                                                <th width="5%">Sl.No</th>
                                                <th width="5%">Category</th>
                                                <th width="10%">Course</th>
                                                <th width="10%">Subject</th>
                                                <th width="10%">University</th>
                                                <th width="10%">Pass Out Year</th>
                                                <th width="10%">Mark</th>
                                            </tr>
                                            </thead>
                                            <tbody class="my-list">
                                        <?php
                                        $count=1;
                                        foreach($candidate_education as $u){
                                        ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $u['qualfcn_catgry']; ?></td>
                                                <td><?php echo $u['course']; ?></td>
                                                <td><?php echo $u['subject']; ?></td>
                                                <td><?php echo $u['university']; ?></td>
                                                <td><?php echo $u['pass_year']; ?></td>
                                                <td><?php echo $u['mark']; ?></td>
                                            </tr>
                                        <?php $count++;} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

    

        
       
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="<?php echo base_url(); ?>assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>dist/js/pages/mask/mask.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/quill/dist/quill.min.js"></script>
    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        /*colorpicker*/
        $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

    </script>
</body>

</html>