<?php
ob_start();
class Admin_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
       
    }
    public function login()
    {
        $username=$this->input->post('username');
        $password=$this->input->post('pass');
        $query=$this->db->get_where('login_users',array('username'=>$username,'password'=>$password,'type'=>'admin'));
        if($query->num_rows()>0)
        {
            foreach($query->result() as $details)
            $cand_data=array(
                            'id'=>$details->id,
                            'username'=>$details->username,
                        );
            $this->session->set_userdata($cand_data);
            return true;
        }
        else
        {
            return false;
        }
    }
    public function get_candidate_list()
    {
        $this->db->select('*');
        $this->db->FROM('cand_registration');
        $this->db->group_by('cand_id');     
        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        
        return $query;
    }
    public function get_candidate_details($cand_id)
    {
         $this->db->select('A.*,B.*,C.*');
        $this->db->FROM('cand_registration A');
        $this->db->join('cand_profile B', 'A.cand_id=B.cand_id','left');
        $this->db->join('candidate_edu_details C', 'B.cand_id=C.cand_id','left');              
        $this->db->where('A.cand_id',$cand_id);
        $query = $this->db->get()->result_array();
        
        // print_r($this->db->last_query());die();
        
        return $query;
    }
    public function get_candidate_education($cand_id)
    {
        $this->db->select('*');
        $this->db->FROM('candidate_edu_details');
        $this->db->where('cand_id',$cand_id);

        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        
        return $query;
    }
    public function delete_candidate_details($cand_id) 
    {
        $this->db->delete('cand_registration', array('cand_id' => $cand_id));
        $this->db->delete('candidate_edu_details', array('cand_id' => $cand_id)); 
        return;
    }
    public function approve_candidate($cand_id) 
    {
        $data['status']   = 1;
        $this->db->update('cand_registration', $data, array('cand_id'=>$cand_id));
        return;
    }
     function save_question_entry()
    {
        $data['topic_id']  = $this->input->post('topic_id');
        $data['ques_name'] = $this->input->post('ques_name');
        $data['weightage']  = $this->input->post('weightage');
        $data['question_type']  = $this->input->post('question_type');
        $this->db->insert('question_master',$data);
        $question_id = $this->db->insert_id();
        if($data['question_type']  == "passage")
        {
            $row_no = $this->input->post('row_no');
            $sub_ques_name = $this->input->post('sub_ques_name');
            $sub_option1 = $this->input->post('sub_option1');
            $sub_option2 = $this->input->post('sub_option2');
            $sub_option3 = $this->input->post('sub_option3');
            $sub_option4 = $this->input->post('sub_option4');
            $sub_answer = $this->input->post('sub_answer');
            for($i=0;$i<$row_no;$i++)
            {
                $data1['question_id'] = $question_id;
                $data1['question'] = $sub_ques_name[$i];
                $data1['option_1'] = $sub_option1[$i];
                $data1['option_2'] = $sub_option2[$i];
                $data1['option_3'] = $sub_option3[$i];
                $data1['option_4'] = $sub_option4[$i];
                $data1['answer'] = $sub_answer[$i]; 
                $this->db->insert('passage_questions',$data1);
            }
        }
        else
        {
            $data2['question_id'] = $question_id;
            $data2['option_1'] = $this->input->post('option_1');
            $data2['option_2'] = $this->input->post('option_2');
            $data2['option_3'] = $this->input->post('option_3');
            $data2['option_4'] = $this->input->post('option_4');
            $data2['answer'] = $this->input->post('answer');
            $this->db->insert('general_questions',$data2);
        }
    }
    public function get_question_master_list() 
    {
        $this->db->select("*");
        $this->db->from("question_master");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_general_question_detail($quest_id)
    {
        $this->db->select('B.*,A.id,A.question_type,A.ques_name');
        $this->db->FROM('question_master A');
        $this->db->join('general_questions B', 'A.id=B.question_id','left');
        $this->db->where(array('A.id' =>  $quest_id,'A.question_type' => 'general'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function get_question_detail($quest_id) 
    {
        $this->db->select("*");
        $this->db->from("question_master");
        $this->db->where("id",$quest_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function get_passage_question_detail($quest_id)
    {
        $this->db->select('B.*,A.id,A.question_type,A.ques_name');
        $this->db->FROM('question_master A');
        $this->db->join('passage_questions B', 'A.id=B.question_id','left');
        $this->db->where(array('A.id' =>  $quest_id,'A.question_type' => 'passage'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function update_question_entry($quest_id,$question_type)
    {
        $question_type = $this->input->post('question_type');
        $this->db->select('*');
        $this->db->from("question_master");
        $this->db->where("id",$quest_id);
        $query = $this->db->get();
        $result = $query->num_rows();
        if($result > 0)
        {
            if($question_type == 'general')
            {
                $data['question_id'] = $quest_id;
                $data['answer'] = $this->input->post('answer');
                $data['option_1'] = $this->input->post('option_1');
                $data['option_2'] = $this->input->post('option_2');
                $data['option_3'] = $this->input->post('option_3');
                $data['option_4'] = $this->input->post('option_4');
                $this->db->update('general_questions', $data, array('question_id' =>  $quest_id));
            }
            else
            {
                $row_no = $this->input->post('row_no');
                $sub_ques_name = $this->input->post('sub_ques_name');
                $sub_option1 = $this->input->post('sub_option1');
                $sub_option2 = $this->input->post('sub_option2');
                $sub_option3 = $this->input->post('sub_option3');
                $sub_option4 = $this->input->post('sub_option4');
                $sub_answer = $this->input->post('sub_answer');
                $id = $this->input->post('q_id');
                for($i=0;$i<$row_no;$i++)
                {
                    $data1['question_id'] = $quest_id;
                    $data1['question'] = $sub_ques_name[$i];
                    $data1['option_1'] = $sub_option1[$i];
                    $data1['option_2'] = $sub_option2[$i];
                    $data1['option_3'] = $sub_option3[$i];
                    $data1['option_4'] = $sub_option4[$i];
                    $data1['answer'] = $sub_answer[$i]; 
                    $this->db->update('passage_questions', $data1, array('question_id' =>  $quest_id, 'passage_id' => $id));
                }
            }
            $data2['topic_id']  = $this->input->post('topic_id');
            $data2['weightage']  = $this->input->post('weightage');
            $data2['question_type']  = $this->input->post('question_type');
            $data2['ques_name'] = $this->input->post('ques_name');
            $this->db->update('question_master', $data2, array('id' =>  $quest_id));
        }
    }
    public function delete_question_entry($quest_id) 
    {
        $this->db->delete('question_master', array('id' => $quest_id)); 
        return;
    }
}
?>