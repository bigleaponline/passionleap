<?php
ob_start();
class Passion_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
       
    }
    function candidate_Registration()
    {
        $username = $this->input->post('username');
        $email    = $this->input->post('email');
        $password=$this->input->post('mobile');
        $pieces = explode(" ", $password);
        $code= $pieces[0];
        $mobile= $pieces[1];
        $data=array(
                    'name'=>$this->input->post('name'),
                    'email'=>$email,
                    'dob'=>$this->input->post('dob'),
                    'stream'=>$this->input->post('stream'),
                    'countrycode'=>$code,
                    'contactnumber'=>$mobile,
                    'status'=>0,
                    'college'=>$this->input->post('college'),
                    'roll_no'=>$username,
                    'year'=>$this->input->post('year'),
                    'password'=>base64_encode($mobile),
                    'cand_test_status'=>0,
                    'date'=>date('d-m-Y')
                    
                    );
        $insert=$this->db->insert('cand_registration',$data);
        if($insert)
        {
            $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'TLS://mail.passionleap.in',
                            'smtp_port' => 465,
                            'smtp_user' => 'no-replay@passionleap.in',
                            'smtp_pass' => ')P*hoqW5~S#;',
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1',
                            'wordwrap' => TRUE
                        );
                        //smtp.gmail.com
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $message="Thank you for registering with PassionLeap .Your credentials are given below\r\n UserID:".$username."\r\n And Password:".$mobile;
            $this->email->from('no-replay@passionleap.in');
            $this->email->to($email); 
            $this->email->subject('PassionLeap');
            $this->email->message($message);  
            // if($this->email->send())
            // {
            //     // echo $message;exit();
            //     echo "send successfully";
            // }
            // else
            // {
            //     show_error($this->email->print_debugger());
            // }
            // return true;
        }
        else
        {
            return false;
        }
    }
    function login()
    {
        $roll_no=$this->input->post('roll_no');
        $password=base64_encode($this->input->post('password'));
        $query=$this->db->get_where('cand_registration',array('roll_no'=>$roll_no ,'password'=>$password));
        if($query->num_rows()>0)
        {
            $status=$query->row()->status;
            if($status==0 || $status==2)
            {
               foreach($query->result() as $details)
                $cand_data=array(
                                'cand_id'=>$details->cand_id,
                                'name'=>$details->name,
                                'stream'=>$details->stream,               
                                'status'=>$details->status,                   
                                'roll_no'=>$details->roll_no,
                                'cand_test_status'=>$details->cand_test_status
                                
                            );
                return $cand_data;
            }
            else
            {
                $this->session->set_flashdata('approve','You have to wait for the approvement');
                 return false;
            }
        }
        else
        {
            $this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
            return false;
        }
    }
    function newsLetter()
    {
        $data=array('email'=>$this->input->post('email'),
                    'date'=>date('Y-m-d')
                    );
        $members=$this->db->insert('tbl_members',$data);
        if($members)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function get_question_list($cand_level='',$topic_id='')
    {
        if($cand_level == 1)
        {
           $topic_id=1;
            $question_type="general";
            $array = array('topic_id' => $topic_id , 'question_type' => $question_type , 'weightage' => $cand_level);
        }
        if($cand_level == 2)
        {
            //$topic_id   = 2;
            $array      = array('topic_id' => $topic_id , 'weightage' => $cand_level);
        }
         
            $this->db->select("*");
            $this->db->from("question_master");
            $this->db->where($array);
            $this->db->order_by("id", "random");
            $this->db->limit(10);   
            $query = $this->db->get()->result_array();
           // print_r($this->db->last_query());exit;
            return $query;
    }
    
     function save_test_answer() 
    {
        //$cand_category=$this->session->userdata['cand_data']['cand_category'];
        $start_time    = $this->session->userdata('start_time');
        $end_time      = $this->session->userdata('end_time');
        $start_time    = new DateTime($start_time);
        $end_time      = new DateTime($end_time);
        $test_time     = $start_time->diff($end_time);
        $test_time     = $test_time->format("%H:%I:%S");
        $ccount         = $this->input->post('ccount');   
        $candidate_id   = $this->input->post('cand_id'); 
        $topic_id   = $this->input->post('topic_id');
        $test_date      = date('Y-m-d');    
        $total          = 0;
        $data1['candidate_id']  = $candidate_id;    
        $data1['test_date']     = $test_date;
        $data1['test_time']     = $test_time;
        $data1['topic_id']     = $topic_id;
        $test_master=$this->db->insert('test_master',$data1);
               // print_r($this->db->last_query());die();

        if($test_master)
        {
            $test_id = $this->db->insert_id();
            $question_type_arr   = $this->input->post('question_type');
            $question_id_arr     = $this->input->post('question_id');
            for($i=1;$i<=$ccount;$i++)
            {
                $question_id      =$question_id_arr[$i-1];
                $question_type    =$question_type_arr[$i-1];
                $data['test_id']  = $test_id;
                $data['quest_id'] = $question_id;
                $answer           = $this->input->post('quest_optn_'.$i);
                $data['answer']   = $answer;
                if($answer!=' ' && $question_type == 'general')
                {
                    $this->db->select('answer');
                    $query = $this->db->get_where('general_questions',array('question_id' => $question_id));
                    $currect_answer=$query->row()->answer;
                    if($currect_answer == $answer)
                    {            
                        $total = $total +1;
                    }  
                    $this->db->insert('test_answer', $data);      
                }
                if($answer!=' ' && $question_type == 'passage')
                {
                    $subquestion_id_arr     = $this->input->post('passage_id');                
                    $subq_count = $this->input->post('subq_count'); 
                    for($j=1;$j<=$subq_count;$j++) 
                    {
                        $subquest_id         = $subquestion_id_arr[$j-1];
                        $subq_answer         = $this->input->post('subquest_optn_'.$j);
                        $data2['test_id']    = $test_id;
                        $data2['quest_id']   = $question_id;
                        $data2['subquest_id'] = $subquest_id;
                        $data2['answer']      = $subq_answer;
                        if($answer!=' ')
                        {
                            $this->db->select('answer');
                            $query = $this->db->get_where('passage_questions',array('passage_id' => $subquest_id));
                            $currect_answer=$query->row()->answer;
                            if($currect_answer == $subq_answer)
                            {            
                                $total = $total +1;
                            }  
                            $this->db->insert('tbl_test_answer', $data2);
                        }
                    }
                }
            }
            $data1['total_score']   = $total;
            $this->db->update('test_master', $data1, array('test_id'=>$test_id));
            // if($topic_id == 1){
            // $data4['cand_test_status']=1;
            // }
            // else{
            $data4['cand_test_status']=$topic_id;
           // }
            
          
            
            $this->db->update('cand_registration', $data4, array('cand_id'=>$candidate_id));
            $this->session->unset_userdata('start_time');
            $this->session->unset_userdata('end_time');
        }
    }
    
    
    
    function get_score($cand_id,$topic_id)
    {
        
        $this->db->select('total_score');
        $this->db->from('test_master');
        $this->db->where(array('candidate_id' => $cand_id, 'topic_id' => $topic_id));
        $query = $this->db->get()->result_array();
        if($query)
        return $query;
        else
        return 0;
    
    }
    
    
    
    function get_cand_qualification($cand_id)
    {
        
        $this->db->select('stream');
        $this->db->from('cand_registration');
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
                // print_r($this->db->last_query());die();

        return $query;
    
    }
    
    
    
    public function get_general_question()
    {
        $question_type="general";
        $array = array('question_type' => $question_type);
        $this->db->select("*");
        $this->db->from("question_master");
        $this->db->where($array);
        $this->db->order_by("id", "random");
        $this->db->limit(20);   
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function save_aptitude_result()
    {
        $candidate_id   =1;
        $test_date      = date('Y-m-d');
        $ccount = 20;
        $total = 0;
        $data['candidate_id']  = $candidate_id;    
        $data['test_date']     = $test_date;
        $test_master=$this->db->insert('test_master',$data);
        if($test_master)
        {
            $test_id = $this->db->insert_id();
            $question_type_arr   = $this->input->post('question_type');
            $question_id_arr     = $this->input->post('question_id');
            for($i=1;$i<=$ccount;$i++)
            {
                $question_id      =$question_id_arr[$i-1];
                $question_type    =$question_type_arr[$i-1];
                $data1['test_id']  = $test_id;
                $data1['quest_id'] = $question_id;
                $answer           = $this->input->post('quest_optn_'.$i);
                $data1['answer']   = $answer;
                if($answer!=' ')
                {
                    $this->db->select('answer');
                    $query = $this->db->get_where('general_questions',array('question_id' => $question_id));
                    $currect_answer=$query->row()->answer;
                    if($currect_answer == $answer)
                    {            
                        $total = $total +1;
                    }  
                    $this->db->insert('test_answer', $data1);      
                }
            }
            $data['total_score']   = $total;
            $this->db->update('test_master', $data, array('test_id'=>$test_id));
        }
    }
    function get_report($cand_id)
    {
        $this->db->select("*");
        $this->db->from('test_master');
        //$this->db->join('tbl_topic_master B', 'A.topic_id=B.topic_id','left');
        $this->db->where(array('candidate_id' => $cand_id));
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_id()
    {
        
        $this->db->select_max("cand_id");
        $this->db->from('cand_registration');
        $query = $this->db->get()->result_array();
        $result = $query[0]['cand_id']; 
        if($result == '')
        {
            $result = 'PS0001000';
        }
        else
        {
            return $result;
        }
    }
    
    public function get_cand_status($cand_id)
    {
        
        $this->db->select("status");
        $this->db->from('cand_registration');
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
        $result = $query[0]['status']; 
       
            return $result;
        
    }
     public function get_cand_test_status($cand_id)
    {
        
        $this->db->select("cand_test_status");
        $this->db->from('cand_registration');
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
        $result = $query[0]['cand_test_status']; 
       
            return $result;
        
    }
    
    public function get_cand_details($cand_id)
    {
        $this->db->select("*");
        $this->db->from('cand_registration');
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
        //print_r($this->db->last_query());die();
        return $query;
    }

    public function profile_registration($image,$signature)
    {
        $data=array(
                    'cand_id'=>$this->input->post('cand_id'),
                    'first_name'=>$this->input->post('fname'),
                    'last_name'=>$this->input->post('lname'),
                    'email'=>$this->input->post('email'),
                    'gender'=>$this->input->post('gender'),
                    'address_line1'=>$this->input->post('address_line1'),
                    'address_line2'=>$this->input->post('address_line2'),
                    'city'=>$this->input->post('city'),
                    'zip_code'=>$this->input->post('zip_code'),
                    'state'=>$this->input->post('state'),
                    'country'=>$this->input->post('country'),
                    'father_name'=>$this->input->post('father_name'),
                    'father_occupation'=>$this->input->post('father_occupation'),
                    'mother_name'=>$this->input->post('mother_name'),
                    'mother_occupation'=>$this->input->post('mother_occupation'),
                    'course_applied'=>$this->input->post('course_applied'),
                    'profile_image'=>$image,
                    'signature'=>$signature,
                    // 'profile_image'=>$this->input->post('profile_img'),
                    // 'signature'=>$this->input->post('sign'),
                    // 'declaration'=>$this->input->post('declaration'),
                    'contact_number'=>$this->input->post('number1'),
                    'alternate_number'=>$this->input->post('number2'),
                    'date'=>date('d-m-Y'),
                    'password'=>base64_encode($this->input->post('password'))
                    );
        $insert=$this->db->insert('cand_profile',$data);
        
        if($insert)
        {
            $data2['status']= 2;
            $data2['password']= base64_encode($this->input->post('password'));
            $this->db->where('cand_id',$this->input->post('cand_id'));
            $this->db->update('cand_registration',$data2);
            
            $cand_id = $this->db->insert_id();
            $row_no  = 3;
            $qualfcn_catgry  = $this->input->post('qualfcn_catgry');
            $course  = $this->input->post('course');
            $subject  = $this->input->post('subject');
            $university  = $this->input->post('college');
            $pass_year  = $this->input->post('pass_year');
            $mark  = $this->input->post('mark');
            for($i=0;$i<$row_no;$i++)
            {
                $data1['qualfcn_catgry'] = $qualfcn_catgry[$i];
                $data1['course'] = $course[$i];
                $data1['subject'] = $subject[$i];
                $data1['university'] = $university[$i];
                $data1['pass_year'] = $pass_year[$i];
                $data1['mark '] = $mark [$i];
                $data1['cand_id'] = $cand_id;
                $candedu=$this->db->insert('candidate_edu_details',$data1);
            }
            // $data2['status']= 2;
            // $data2['password']= base64_encode($this->input->post('password'));
            // $this->db->where('cand_id',$this->input->post('cand_id'));
            // $this->db->update('cand_registration',$data2);
            return true;
        }
        else
        {
            return false;
        }
    }


    function getState()
    {
        if (! empty($_POST["country_id"])) 
        {
            $query = $this->db->get_where('states',array('country_id'=>$_POST["country_id"]));
            $results = $query->result();
            ?>
            <option value disabled selected>Select State</option>
            <?php
            foreach ($results as $state) 
            {
            ?>
                <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                <?php
            }
        }
    }

    function getCity()
    {
        if (! empty($_POST["state_id"])) 
        {
            $query =$this->db->get_where('cities',array('state_id'=>$_POST['state_id']));
            $results =$query->result();
            ?>
            <option value disabled selected>Select City</option>
            <?php
            foreach ($results as $city) 
            {
                ?>
                <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                <?php
            }
        }
    }

    

}
?>