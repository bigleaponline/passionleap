<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'passion_controller';
$route['home']='passion_controller/loadPages/home';
$route['registration_old']='passion_controller/loadPages/registration_old';
$route['service']='passion_controller/loadPages/service';
$route['icep']='passion_controller/loadPages/icep';
$route['video']='passion_controller/loadPages/video';
$route['video']='passion_controller/loadPages/video1';
$route['capitalmarket']='passion_controller/loadPages/capitalmarket';
$route['cybersecurity']='passion_controller/loadPages/cybersecurity';
$route['jobfair-2004']='passion_controller/loadPages/jobfair-2004';
$route['jobfair-2005']='passion_controller/loadPages/jobfair-2005';
$route['jobfair-2006']='passion_controller/loadPages/jobfair-2006';
$route['jobfair-2011']='passion_controller/loadPages/jobfair-2011';
$route['jobfair-2013']='passion_controller/loadPages/jobfair-2013';
$route['jobfair-2014']='passion_controller/loadPages/jobfair-2014';
$route['jobfair-2015']='passion_controller/loadPages/jobfair-2015';
$route['jobfair-2018']='passion_controller/loadPages/jobfair-2018';
$route['jobfair-2020']='passion_controller/loadPages/jobfair-2020';
$route['instruction']='passion_controller/loadPages/instruction';
$route['register']='passion_controller/register';
$route['login']='passion_controller/loadPages/login';
//$route['login']='passion_controller/loadPages/instruction';
$route['logout']='passion_controller/logout';
$route['aptitude']='passion_controller/technical_assesment/';
$route['aptitude/1']='passion_controller/technical_assesment/1';
$route['technical_assesment/2']='passion_controller/technical_assesment/2';
$route['technical_assesment/3']='passion_controller/technical_assesment/3';
$route['technical_assesment/4']='passion_controller/technical_assesment/4';

$route['report']='passion_controller/report';
$route['report_pdf']='passion_controller/technical_report_pdf';
$route['profile_registration']='passion_controller/loadPages/register-final';
$route['register_final']='passion_controller/second_register';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
