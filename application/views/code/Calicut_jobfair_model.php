<?php
ob_start();
class Calicut_jobfair_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
    }
    function get_question_list($cand_level='',$topic_id='')
    {
        if($cand_level == 1)
        {
            $topic_id=1;
            $question_type="general";
            $array = array('topic_id' => $topic_id , 'question_type' => $question_type , 'weightage' => $cand_level);
        }
        if($cand_level == 2)
        {
            $topic_id   = 2;
            $array      = array('topic_id' => $topic_id , 'weightage' => $cand_level);
        }
            $this->db->select("*");
            $this->db->from("tbl_question_master");
            $this->db->where($array);
            $this->db->order_by("id", "random");
            $this->db->limit(20);   
            $query = $this->db->get()->result_array();
            return $query;
    }
    function save_test_answer() 
    {
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $start_time    = $this->session->userdata('start_time');
        $end_time      = $this->session->userdata('end_time');
        $start_time    = new DateTime($start_time);
        $end_time      = new DateTime($end_time);
        $test_time     = $start_time->diff($end_time);
        $test_time     = $test_time->format("%H:%I:%S");
        $ccount         = $this->input->post('ccount');   
        $candidate_id   = $this->input->post('cand_id');
        $topic_id   = $this->input->post('topic_id');
        $test_date      = date('Y-m-d');    
        $total          = 0;
        $data1['candidate_id']  = $candidate_id;    
        $data1['test_date']     = $test_date;
        $data1['test_time']     = $test_time;
        $data1['topic_id']     = $topic_id;
        $test_master=$this->db->insert('tbl_test_master',$data1);
        if($test_master)
        {
            $test_id = $this->db->insert_id();
            $question_type_arr   = $this->input->post('question_type');
            $question_id_arr     = $this->input->post('question_id');
            for($i=1;$i<=$ccount;$i++)
            {
                $question_id      =$question_id_arr[$i-1];
                $question_type    =$question_type_arr[$i-1];
                $data['test_id']  = $test_id;
                $data['quest_id'] = $question_id;
                $answer           = $this->input->post('quest_optn_'.$i);
                $data['answer']   = $answer;
                if($answer!=' ' && $question_type == 'general')
                {
                    $this->db->select('answer');
                    $query = $this->db->get_where('tbl_general_questions',array('question_id' => $question_id));
                    $currect_answer=$query->row()->answer;
                    if($currect_answer == $answer)
                    {            
                        $total = $total +1;
                    }  
                    $this->db->insert('tbl_test_answer', $data);      
                }
                if($answer!=' ' && $question_type == 'passage')
                {
                    $subquestion_id_arr     = $this->input->post('passage_id');                
                    $subq_count = $this->input->post('subq_count'); 
                    for($j=1;$j<=$subq_count;$j++) 
                    {
                        $subquest_id         = $subquestion_id_arr[$j-1];
                        $subq_answer         = $this->input->post('subquest_optn_'.$j);
                        $data2['test_id']    = $test_id;
                        $data2['quest_id']   = $question_id;
                        $data2['subquest_id'] = $subquest_id;
                        $data2['answer']      = $subq_answer;
                        if($answer!=' ')
                        {
                            $this->db->select('answer');
                            $query = $this->db->get_where('tbl_passage_questions',array('passage_id' => $subquest_id));
                            $currect_answer=$query->row()->answer;
                            if($currect_answer == $subq_answer)
                            {            
                                $total = $total +1;
                            }  
                            $this->db->insert('tbl_test_answer', $data2);
                        }
                    }
                }
            }
            $data1['total_score']   = $total;
            $this->db->update('tbl_test_master', $data1, array('test_id'=>$test_id));
            if($topic_id == 1){
            $data4['cand_test_status']=1;
            }
            else{
            $data4['cand_test_status']=2;
            }
            
           if($topic_id == 1){ 
            if($cand_category == 'Normal')
            {
                $data4['hallticket_category'] = 'P';
                
            }
            elseif($cand_category == 'A' && $data1['total_score'] <= 12)
            {
                $data4['hallticket_category'] = 'Q';
                
            }
            elseif($cand_category == 'A' && $data1['total_score'] > 12)
            {
                $data4['hallticket_category'] = 'R';
                
            }
            elseif($cand_category == 'B' && $data1['total_score'] <= 12)
            {
                $data4['hallticket_category'] = 'S';
                
            }
            elseif($cand_category == 'B' && $data1['total_score'] > 12)
            {
                $data4['hallticket_category'] = 'T';
                
            }
            elseif($cand_category == 'C')
            {
                $data4['hallticket_category'] = 'U';
                
            }
            elseif($cand_category == 'D')
            {
                $data4['hallticket_category'] = 'V';
                
            }
           }
            
            $this->db->update('tbl_candidate_registration', $data4, array('cand_id'=>$candidate_id));
            $this->session->unset_userdata('start_time');
            $this->session->unset_userdata('end_time');
        }
    }
    function get_report($cand_id)
    {
        $this->db->select("A.*,B.topic_name");
        $this->db->from('tbl_test_master A');
        $this->db->join('tbl_topic_master B', 'A.topic_id=B.topic_id','left');
        $this->db->where(array('candidate_id' => $cand_id));
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_cand_qualification($cand_id)
    {
        $this->db->select("A.*,B.qualification,C.stream_name");
        $this->db->from("tbl_candidate_registration A");
        $this->db->join("tbl_qualificatio_master B", "A.cand_qualification=B.qualification_id","left");
        $this->db->join("tbl_stream_master C", "A.cand_stream=C.stream_id","left");
        $this->db-> where(array('A.cand_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        return $query;
    }
    function get_quantitative_score($cand_id)
    {
        $this->db->select('tbl_test_master.total_score');
        $this->db->from('tbl_test_master');
        $this->db->where(array('candidate_id' => $cand_id, 'topic_id' => 2));
        $query = $this->db->get()->result_array();
        return $query;
    
    }
    function get_reasoning_score($cand_id)
    {
        $this->db->select('tbl_test_master.total_score');
        $this->db->from('tbl_test_master');
        $this->db->where(array('candidate_id' => $cand_id, 'topic_id' => 3));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_verbal_score($cand_id)
    {
        $this->db->select('tbl_test_master.total_score');
        $this->db->from('tbl_test_master');
        $this->db->where(array('candidate_id' => $cand_id, 'topic_id' => 4));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_assessment_details()
    {
        $cand=$this->input->post('cand_id');
        $query=$this->db->get_where('tbl_test_master',array('candidate_id'=>$cand))->result();
        if($query)
        {
            echo $query;
        }
        else
        {
            return false;
        }
    }
    function get_test_master($candid)
    {
        $query=$this->db->get_where('tbl_test_master',array('candidate_id'=>$candid));
        if($query->num_rows()>0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    function get_candidate_details($cand_id)
    {
        $this->db->select('A.*,B.qualification,C.stream_name');
        $this->db->from('tbl_candidate_registration A');
        $this->db->join("tbl_qualificatio_master B", "A.cand_qualification=B.qualification_id","left");
        $this->db->join("tbl_stream_master C", "A.cand_stream=C.stream_id","left");
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
        return $query; 
    }
    function get_technical_report($cand_id)
    {
        $this->db->select("A.*,B.topic_name");
        $this->db->from('tbl_test_master A');
        $this->db->join('tbl_topic_master B', 'A.topic_id=B.topic_id','left');
        $this->db->where(array('candidate_id' => $cand_id));
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_cand_image($cand_id)
    {
        $this->db->select('cand_img');
        $this->db->from('tbl_candidate_profile');
        $this->db->where(array('candidate_id' => $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_normal_cand_image($cand_id)
    {
        $this->db->select('cand_image');
        $this->db->from('tbl_candidate_registration');
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function get_technical_questions()
    {
    
        $this->db->select("*");
        $this->db->from("tbl_question_master");
        $this->db->where('weightage', 2);
        $this->db->order_by("id", "random");
        $this->db->limit(20);   
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function save_technicaltest_answer() 
    {
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $start_time    = $this->session->userdata('start_time');
        $end_time      = $this->session->userdata('end_time');
        $start_time    = new DateTime($start_time);
        $end_time      = new DateTime($end_time);
        $test_time     = $start_time->diff($end_time);
        $test_time     = $test_time->format("%H:%I:%S");
        $ccount         = $this->input->post('ccount');   
        $candidate_id   = $this->input->post('cand_id');
        $topic_id   = 2;
        $test_date      = date('Y-m-d');    
        $total          = 0;
        $data1['candidate_id']  = $candidate_id;    
        $data1['test_date']     = $test_date;
        $data1['test_time']     = $test_time;
        $data1['topic_id']     = $topic_id;
        $test_master=$this->db->insert('tbl_test_master',$data1);
        if($test_master)
        {
            $test_id = $this->db->insert_id();
            $question_type_arr   = $this->input->post('question_type');
            $question_id_arr     = $this->input->post('question_id');
            for($i=1;$i<=$ccount;$i++)
            {
                $question_id      =$question_id_arr[$i-1];
                $question_type    =$question_type_arr[$i-1];
                $data['test_id']  = $test_id;
                $data['quest_id'] = $question_id;
                $answer           = $this->input->post('quest_optn_'.$i);
                $data['answer']   = $answer;
                if($answer!=' ' && $question_type == 'general')
                {
                    $this->db->select('answer');
                    $query = $this->db->get_where('tbl_general_questions',array('question_id' => $question_id));
                    $currect_answer=$query->row()->answer;
                    if($currect_answer == $answer)
                    {            
                        $total = $total +1;
                    }  
                    $this->db->insert('tbl_test_answer', $data);      
                }
                if($answer!=' ' && $question_type == 'passage')
                {
                    $subquestion_id_arr     = $this->input->post('passage_id');                
                    $subq_count = $this->input->post('subq_count'); 
                    for($j=1;$j<=$subq_count;$j++) 
                    {
                        $subquest_id         = $subquestion_id_arr[$j-1];
                        $subq_answer         = $this->input->post('subquest_optn_'.$j);
                        $data2['test_id']    = $test_id;
                        $data2['quest_id']   = $question_id;
                        $data2['subquest_id'] = $subquest_id;
                        $data2['answer']      = $subq_answer;
                        if($answer!=' ')
                        {
                            $this->db->select('answer');
                            $query = $this->db->get_where('tbl_passage_questions',array('passage_id' => $subquest_id));
                            $currect_answer=$query->row()->answer;
                            if($currect_answer == $subq_answer)
                            {            
                                $total = $total +1;
                            }  
                            $this->db->insert('tbl_test_answer', $data2);
                        }
                    }
                }
            }
            $data1['total_score']   = $total;
            $this->db->update('tbl_test_master', $data1, array('test_id'=>$test_id));

            $data4['cand_test_status']=3;
            
            $this->db->update('tbl_candidate_registration', $data4, array('cand_id'=>$candidate_id));
            $this->session->unset_userdata('start_time');
            $this->session->unset_userdata('end_time');
        }
    }
}
?>