<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calicut_jobfair_controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('calicut_jobfair_model');
        $this->load->model('jobfair_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('jobfair_helper');
        $this->load->library('pdf');
    }
    public function loadpage()
    {
        $page=$this->uri->segment('2');
        $data['page_title']=$this->uri->segment('2');
        $this->load->view('thrissur/calicut_header',$data);
        $this->load->view($page);
        $this->load->view('thrissur/calicut_footer');
    }
    public function signin()
	{
		$data['qualification']=$this->jobfair_model->signin();
		$data['year']=$this->jobfair_model->year();
		$data['page_title']=$this->uri->segment('1');
		$this->load->view('thrissur/calicut_header',$data);
		$this->load->view('sign-in',$data);
		$this->load->view('thrissur/calicut_footer');
	}
    public function assesment_test()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $testmaster=$this->calicut_jobfair_model->get_test_master($cand_id);
        if($testmaster)
        {
            $cand_level         = 1;
            $data['cand_id']    = $this->session->userdata['cand_data']['cand_id'];
            $data['cand_level'] = $cand_level;      
            $topic_id           = 1;
            $data['topic_id']   = $topic_id;
            $data['page_title']=$this->uri->segment('2');
            $data['question_list'] = $this->calicut_jobfair_model->get_question_list($cand_level,$topic_id);
            $start_time = date('H:i:s');
            $this->session->set_userdata('start_time',$start_time);
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/general_assesment',$data);
            $this->load->view('thrissur/calicut_footer');         
        }
        else
        {
            redirect('logout');
        }
	}
    public function assesment_test_submission()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $topic_id=$this->uri->segment('3');
        $cand_level=$this->session->userdata['cand_data']['cand_level'];
        $end_time = date('H:i:s');
        $this->session->set_userdata('end_time',$end_time);
        $this->calicut_jobfair_model->save_test_answer();
        if($topic_id == 2)
        {
            redirect('thrissur/technical_assesment/3');
        }
        elseif ($topic_id == 3) 
        {
          redirect('thrissur/technical_assesment/4');
        }
        else
        { 
            if ($topic_id == 1) 
            {
                redirect("thrissur/report");
            }
            if($topic_id == 4)
            {
                 redirect("thrissur/technical_report");
            }
        }
    }
    public function report()
    {
        $cand_id              =$this->session->userdata['cand_data']['cand_id'];
        $report['result']     = $this->calicut_jobfair_model->get_report($cand_id);
        $result=$this->calicut_jobfair_model->get_report($cand_id);
        if($result)
        {
            $report['cand_id']    = $cand_id;
            $report['cand_level'] = $this->session->userdata['cand_data']['cand_level']; 
            $data['page_title']   = $this->uri->segment('2'); 
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/test_report',$report);
            $this->load->view('thrissur/calicut_footer');
        }
        else
        {
            redirect('logout');
        }
    }
    public function technical_assesment()
    {
        $status=$this->session->userdata['cand_data']['cand_test_status'];
        $category=$this->session->userdata['cand_data']['cand_category'];
        if($status!=2 && $category=='D' || $category=='C' || $category=='B')
        {
            $topic_id=$this->uri->segment('3'); 
            if($topic_id == "")
            $topic_id = 2;
           
            $cand_level=$this->session->userdata['cand_data']['cand_level'];
            $data['cand_id'] = $this->session->userdata['cand_data']['cand_id'];
            $data['cand_level'] = $cand_level;
            $data['topic_id'] = $topic_id;
            $data['question_list'] = $this->calicut_jobfair_model->get_question_list($cand_level,$topic_id);
            $start_time = date('H:i:s');
            $data['page_title']   = $this->uri->segment('2');
            $this->session->set_userdata('start_time',$start_time);   
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/assesment',$data);
            $this->load->view('thrissur/calicut_footer');   
        }
        else
        {
            redirect('logout');
        }
    }
    public function technical_report()
    {
        if($this->session->userdata('cand_data'))
        {
            $cand_id           =$this->session->userdata['cand_data']['cand_id'];
            $report['cand_id'] = $cand_id;
            $cand_name           =$this->session->userdata['cand_data']['name'];
            $report['name'] = $cand_name;
            $report['qualification'] = $this->calicut_jobfair_model->get_cand_qualification($cand_id);
            $report['cand_level'] = $this->session->userdata['cand_data']['cand_level'];
            $report['quantitative_score']  = $this->calicut_jobfair_model->get_quantitative_score($cand_id);
            $report['reasoning_score']  = $this->calicut_jobfair_model->get_reasoning_score($cand_id);
            $report['verbal_score']  = $this->calicut_jobfair_model->get_verbal_score($cand_id);
            $data['page_title']=$this->uri->segment('2');
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/technical_report',$report);
            $this->load->view('thrissur/calicut_footer');
        }
        else
        {
            redirect('logout');
        }
    }
    public function get_assessment_details()
    {
        $pro=$this->calicut_jobfair_model->get_assessment_details();
    }
    public function technical_report_pdf()
    {
        $cand_id           =$this->session->userdata['cand_data']['cand_id'];
        $report['cand_id'] = $cand_id;
        $cand_name           =$this->session->userdata['cand_data']['name'];
        $report['name'] = $cand_name;
        $report['qualification'] = $this->calicut_jobfair_model->get_cand_qualification($cand_id);
        $report['cand_level'] = $this->session->userdata['cand_data']['cand_level'];
        $report['chrt_img']       = $this->input->post('chrt_img');
        $report['quantitative_score']  = $this->calicut_jobfair_model->get_quantitative_score($cand_id);
        $report['reasoning_score']  = $this->calicut_jobfair_model->get_reasoning_score($cand_id);
        $report['verbal_score']  = $this->calicut_jobfair_model->get_verbal_score($cand_id);
        $this->pdf->load_view('thrissur/technical_report_pdf',$report);
        $this->pdf->render();
        $this->pdf->stream("technical_report_pdf.pdf");
    }
    public function technical_report_print()
    {
        $cand_id           =$this->session->userdata['cand_data']['cand_id'];
        $report['cand_id'] = $cand_id;
        $cand_name           =$this->session->userdata['cand_data']['name'];
        $report['name'] = $cand_name;
        $report['qualification'] = $this->calicut_jobfair_model->get_cand_qualification($cand_id);
        $report['cand_level'] = $this->session->userdata['cand_data']['cand_level'];
        $report['chrt_img']       = $this->input->post('chrt_img');
        $report['quantitative_score']  = $this->calicut_jobfair_model->get_quantitative_score($cand_id);
        $report['reasoning_score']  = $this->calicut_jobfair_model->get_reasoning_score($cand_id);
        $report['verbal_score']  = $this->calicut_jobfair_model->get_verbal_score($cand_id);
        $this->load->view('thrissur/technical_report_print',$report);
    }
    public function hall_ticket()
    {
        if($this->session->userdata('cand_data'))
        {
            $data['page_title'] =$this->uri->segment('2'); 
            $cand_id            =$this->session->userdata['cand_data']['cand_id'];
            $data['cand_id']    = $cand_id;
            $cand_name          =$this->session->userdata['cand_data']['name'];
            $data['name']       = $cand_name;
            $cand_level         =$this->session->userdata['cand_data']['cand_level'];
            $data['cand_level'] = $cand_level;
            
            $data['candidate_details'] = $this->calicut_jobfair_model->get_candidate_details($cand_id);
            $candidate_details=$this->calicut_jobfair_model->get_candidate_details($cand_id);
            foreach($candidate_details as $cnd)
            if($cand_level == 0)
            {
                $data['normal_cand_image']  = $this->calicut_jobfair_model->get_normal_cand_image($cand_id);
            }
            else
            {
                $data['cand_image']  = $this->calicut_jobfair_model->get_cand_image($cand_id);
            }
            if($cnd['cand_test_status'] == 1)
            {
            $data['general_score'] = $this->calicut_jobfair_model->get_report($cand_id);
             }
            elseif($cnd['cand_test_status'] == 2)
            {
            $data['quantitative_score']  = $this->calicut_jobfair_model->get_quantitative_score($cand_id);
            $data['reasoning_score']  = $this->calicut_jobfair_model->get_reasoning_score($cand_id);
            $data['verbal_score']  = $this->calicut_jobfair_model->get_verbal_score($cand_id);
            }
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/hall_ticket',$data);
            $this->load->view('thrissur/calicut_footer');
        }
        else
        {
            redirect('logout');
        }
    }
    public function hall_ticket_pdf()
    {
        $cand_id           =$this->session->userdata['cand_data']['cand_id'];
        $data['cand_id'] = $cand_id;
        $cand_name           =$this->session->userdata['cand_data']['name'];
        $data['name'] = $cand_name;
        $cand_level           =$this->session->userdata['cand_data']['cand_level'];
        $data['cand_level'] = $cand_level;
        $data['candidate_details'] = $this->calicut_jobfair_model->get_candidate_details($cand_id);
        $candidate_details=$this->calicut_jobfair_model->get_candidate_details($cand_id);
    
        foreach($candidate_details as $cnd)
        if($cand_level == 0)
        {
            $data['normal_cand_image']  = $this->calicut_jobfair_model->get_normal_cand_image($cand_id);
        }
        else
        {
            $data['cand_image']  = $this->calicut_jobfair_model->get_cand_image($cand_id);
        }
        if($cnd['cand_test_status']== 1)
        {
            $data['general_score'] = $this->calicut_jobfair_model->get_report($cand_id);
        }
        elseif($cnd['cand_test_status'] == 2)
        {
            $data['quantitative_score']  = $this->calicut_jobfair_model->get_quantitative_score($cand_id);
            $data['reasoning_score']  = $this->calicut_jobfair_model->get_reasoning_score($cand_id);
            $data['verbal_score']  = $this->calicut_jobfair_model->get_verbal_score($cand_id);
        }
        $this->pdf->load_view('thrissur/hall_ticket_pdf',$data);
        $this->pdf->render();
        $this->pdf->stream("hall_ticket_pdf.pdf");
    }
    
     public function hall_ticket_print()
    {

        $cand_id           =$this->session->userdata['cand_data']['cand_id'];
        $data['cand_id'] = $cand_id;
        $cand_name           =$this->session->userdata['cand_data']['name'];
        $data['name'] = $cand_name;
        $cand_level           =$this->session->userdata['cand_data']['cand_level'];
        $data['cand_level'] = $cand_level;
        $data['candidate_details'] = $this->calicut_jobfair_model->get_candidate_details($cand_id);
        $candidate_details=$this->calicut_jobfair_model->get_candidate_details($cand_id);
        foreach($candidate_details as $cnd)
        if($cand_level == 0)
        {
            $data['normal_cand_image']  = $this->calicut_jobfair_model->get_normal_cand_image($cand_id);
        }
        else
        {
            $data['cand_image']  = $this->calicut_jobfair_model->get_cand_image($cand_id);
        }
        if($cnd['cand_test_status']== 1)
        {
            $data['general_score'] = $this->calicut_jobfair_model->get_report($cand_id);
        }
        elseif($cnd['cand_test_status'] == 2)
        {
            $data['quantitative_score']  = $this->calicut_jobfair_model->get_quantitative_score($cand_id);
            $data['reasoning_score']  = $this->calicut_jobfair_model->get_reasoning_score($cand_id);
            $data['verbal_score']  = $this->calicut_jobfair_model->get_verbal_score($cand_id);
        }

        $this->load->view('thrissur/hall_ticket_print',$data);

    }
}
?>