<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
.col-md-2.new, .col-md-5.new {
  margin-bottom: 20px;
  margin-left: 25px;
}

body{

  font-family: Arial, Helvetica, sans-serif;
}
.head_address{
  padding-left:20px;
  font-size: 12px;
 
  padding-top: 50px;
  width:250px;
  text-align: center;
}
.skills_td{
}
.progress-bar.new {
    background-color: #990099;
}
.progress-bar.new1{

  background-color: #428bca;
}

.progress-bar.new2{

  background-color: #d9534f;
}

.progress-bar.new3{

  background-color: #f0ad4e;
}

.progress-bar.new4{

  background-color: #5cb85c;
}

.col-md-6.skill_title {
    padding: 2% 0 2% 1%;
}

.col-md-6.skill_chart {
    padding: 2% 0 2% 0;
}
.assmnt-clm label { font-weight:normal; }

</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Assessment', 'Marks per Topic'],
    ['Verbal Aptitude',  <?php echo $e_score;?>],
    ['Quantitative Aptitude',     <?php echo $q_score;?>],   
    ['Reasoning',      <?php echo $r_score;?>]
    
  ]);

  var options = {
    title: 'My Mark Distribution',
    is3D: true,
  };
  var chart_area = document.getElementById('piechart_3d');

  var my_chart = new google.visualization.PieChart(chart_area);

  google.visualization.events.addListener(my_chart, 'ready', function(){
  chart_area.innerHTML = '<img src="' + my_chart.getImageURI() + '" class="img-responsive">';

  console.log(chart_area.innerHTML);

});

  my_chart.draw(data, options);
}
</script>
</head>
<body>
<?php
//print_r($qualfcn);exit();
  foreach($qualfcn as $qualification)
  {
    $qualification = $qualification['stream'];
  }
    if($english_score!=0){
      foreach ($english_score as $english_score) {

        $e_score = $english_score['total_score'];
        $e_percent = $e_score * 10;
      }
     }
      else
      {
          $e_score = 0;
        $e_percent = $e_score * 10;
      }
     if($quantitative_score!=0){
          foreach ($quantitative_score as $quantitative_score) {
    
            $q_score = $quantitative_score['total_score'];
            $q_percent = $q_score * 10;
          }
        }
      else
      {
          $q_score = 0;
        $q_percent = $q_score * 10;
      }
      
          
     
     if($reasoning_score!=0){ 
      foreach ($reasoning_score as $reasoning_score) {

        $r_score = $reasoning_score['total_score'];
        $r_percent = $r_score * 10;

      }
        }
      else
      {
        $r_score = 0;
        $r_percent = $r_score * 10;
      }
      
     if($verbal_score!=0){
      foreach ($verbal_score as $verbal_score) {

        $v_score = $verbal_score['total_score'];
        $v_percent = $v_score * 10;

      }
        }
      else
      {
          $v_score = 0;
        $v_percent = $v_score * 10;
      }
?>

<table class="header_tb" width="100%" style="border-left: 1px solid #000;height:auto;border-bottom: none;border-right: 1px solid #000;border-top: 1px solid #000;">
  <tr>
    <td colspan="2" align="center" style="padding-top: 20px;">
      <h2 align="center"><b>Assessment Report</b></h2>
    </td>
  </tr>
  <tr><td colspan="4" height="20px"></td></tr>

  <tr>
    <td><label><b>Candidate Name :</b> <?php echo $name;?></label></td>
    <td><label><b>Candidate Id :</b> <?php echo $cand_id;?></label></td>
  </tr>
  <tr><td colspan="4" height="5px"></td></tr>
  <tr>
    <td><label><b>Qualification :</b> <?php echo $qualification;?></label></td>
  </tr>
  
  <tr>
    <td style="padding-top: 10px;"><h4><b>Skill Distribution</b></h4></td>
  </tr>

   <tr style="padding-bottom: 5px;">
    <td><label>Verbal Aptitude</label></td>
    <td><div data-percentage="0%" style="width: <?php echo $e_percent;?>%;height: 15px;" class="progress-bar new1" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div></td>
    <td><span><?php echo $e_score;?>/10</span></td>
  </tr>
  <tr><td colspan="4" height="5px"></td></tr>

  <tr style="padding-bottom: 5px;">
    <td><label>Quantitative Aptitude</label></td>
    <td><div data-percentage="0%" style="width: <?php echo $q_percent;?>%;height: 15px;" class="progress-bar new2" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div></td>
    <td><span><?php echo $q_score;?>/10</span></td>
  </tr>
  <tr><td colspan="4" height="5px"></td></tr>

  <tr>
    <td><label>ReasoningAptitude</label></td>
    <td><div data-percentage="0%" style="width: <?php echo $r_percent;?>%;height: 15px;" class="progress-bar new3" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div></td>
    <td><span><?php echo $r_score;?>/10</span></td>
  </tr>
  <tr><td colspan="4" height="5px"></td></tr>

    <tr>
    <!--   <td><label>Verbal Aptitude</label></td>
      <td><div data-percentage="0%" style="width: <?php echo $v_percent;?>%;height: 15px;" class="progress-bar new3" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div></td>
      <td><span ><?php echo $v_score;?>/10</span></td>
    </tr> -->
    <tr><td colspan="4" height="5px"></td></tr>

  <tr>
    <table class="header_tb" width="100%" style="border-left: 1px solid #000;height:auto;border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: none;">
    <td><div style="padding-bottom: 10px;"><img src="<?php echo $chrt_img;?>"  id="pdf_img" width="700px;" height="500px;"></div></td>
  </table>
  </tr>
</table>
</body>
</html>