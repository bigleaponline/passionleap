<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->
    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Learn and Lead the World!</h2>
                        <p>PassionLeap offers an array of enriching courses specially designed by academic and industry experts together to offer students the best of academic and industry experience. From curated courses for those seeking to increase their knowledge in data science tools and techniques to value-added cybersecurity and ethical hacking courses, which open up professional possibilities as Ethical Hacker, Forensic Investigator, Information System Security Expert, Mobile Pentester and such, our certification courses in Data Science and IT fields are prospective enough to fetch demanding career opportunities. </p>
						<blockquote class="blockquote">
  
  <footer class="blockquote-footer">Teaching aspirants about surviving and leading capital markets and the fundamental financial area would be our certified course on Capital Market. Besides these, our well-crafted Integrated Career Enrichment Program prepare aspirants for prospective career possibilities, accelerate career growth and help them stand out among the crowd. 
</footer>
</blockquote>
                    </div>
                    <div class="col-md-5 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/about.png" alt="" />
                        </div>
                    </div>
                </div>
				<!--<embed src="../../assets/img/pdf.pdf" width="800px" height="2100px" />-->
				 
		<!--		<img src="<?php echo base_url();?>assets/img/download1.jpg" alt="" id="myImg"/>&nbsp;&nbsp;
	&nbsp;<img src="<?php echo base_url();?>assets/img/download2.jpg" alt=""  id="myImg1"/>&nbsp;&nbsp;&nbsp;
				<img src="<?php echo base_url();?>assets/img/download3.jpg" alt="" id="myImg2"/>
				
				<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
-->

            </div>
        </section>
        <section class="services-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 col-md-12 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="section-title text-center">
                            <h2 class="wow fadeInUp">OUR <span style="color:#f25929">COURSES</span> INCLUDED </h2>
                            <p class="wow fadeInUp">PassionLeap offers an array of enriching courses specially designed by academic and <br>
            industry experts together to offer students the best of academic and industry experience.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.4s">
                        <div class="single-serviecs-block wow fadeInUp">
                            
                              <i class="material-icons">assignment</i>
                            <h3><a href="<?php echo base_url();?>icep">icep</a></h3>
                            <p>Career Enrichment Program (ICEP), is the combination of online
                  videos, practice exams, assessments and mock interviews.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.5s">
                        <div class="single-serviecs-block wow fadeInUp">
                            
                             <i class="material-icons">important_devices</i>
                           <h3><a href="<?php echo base_url();?>capitalmarket">capitalmarket</a></h3>
                            <p>A capital market may be a financial market during which long-term or equity-backed securities are bought and sold.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="0.6s">
                        <div class="single-serviecs-block wow fadeInRight">
                            
                             <i class="material-icons">speaker_notes</i>
                            <h3><a href="<?php echo base_url();?>cybersecurity">cybersecurity</a></h3>
                            <p>It refers to the body of technologies, processes, and practices designed to guard data from attack or unauthorized access.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="clients clientscmn">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">PARTICIPATING <span style="color:#f25929">COMPANIES</span></h2>
      </div>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-06.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-07.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-08.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-09.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-10.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-11.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-12.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-13.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script>
        new WOW().init();

    </script>
       <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });


	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
</body>

</html>
