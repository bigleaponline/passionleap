<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!--========== Specific Meta ==========-->
<meta name="description" content="Passionleap">
<meta name="keywords" content="Passionleap">
<!--======== Page Title===========-->
<title>Passionleap-Register-2</title>
<link rel="shortcut icon" href="images/favicon.ico">
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/style-change.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
<!--[if lt IE 8]>
 	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
<![endif]-->
</head>
<body>
<!--Header Area Start-->
<header id="header">
  <header class="site-header">
    <div class="container">
      <div class="logo-holder"><a href="#"> <img src="<?php echo base_url();?>assets/images/logo-png.png" alt=""></a> </div>
      <div class="right-holder">
        <div class="search-bar">
          <form>
            <input type="text" placeholder="Search..">
            <button type="submit"><span class="search-icon"></span></button>
          </form>
        </div>
        <div class="main-menu">
          <nav class="navbar navbar-expand-md navbar-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation"> <span class="menu-icon"></span> </button>
            <div class="collapse navbar-collapse" id="mainMenu">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item"> <a class="nav-link hvr-underline-from-center" href="http://www.passionleap.in/">Home</a></li>
                <li class="nav-item"> <a class="nav-link hvr-underline-from-center" href="#">Services</a></li>
                <li class="nav-item"> <a class="nav-link hvr-underline-from-center" href="#">Registration</a></li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
<div class="site-banner">
  <div class="container">
    <div class="banner-content wow fadeInUp" data-wow-delay="0.1s"> </div>
  </div>
</div>

<!--<?php //include('include/header.php');?>-->
<!--<div class="site-banner">-->
<!--  <div class="container">-->
<!--    <div class="banner-content wow fadeInUp" data-wow-delay="0.1s"> -->
<!--    </div>-->
<!--  </div>-->
<!--</div>-->
<!--Header Area End-->

<div class="register-page page-wrapper s-pd100 register-02">
  <h2 class="section-heading text-capitalize wow fadeInUp">Complete The Following Details</h2>
  <form action="#" class="reg-form">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">First Name</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your First Name">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Last Name</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Last Name">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Email Address</label>
              <input class="form-control wow fadeInUp" type="email" name="log" id="user_login" placeholder="Enter Your Mail">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area clearfix">
            <p>
              <label class="reglab wow fadeInUp reglabradio">Gender</label>
            <div class="radio wow fadeInUp ">
              <input id="radio-1" name="radio" type="radio" checked>
              <label for="radio-1" class="radio-label">Male</label>
            </div>
            <div class="radio wow fadeInUp ">
              <input id="radio-2" name="radio" type="radio">
              <label  for="radio-2" class="radio-label">Female</label>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Password</label>
              <input class="form-control wow fadeInUp" type="password" name="log" id="user_login" placeholder="Enter Your Password">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Confirm Password</label>
              <input class="form-control wow fadeInUp" type="password" name="log" id="user_login" placeholder="Confirm Password">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Enter Your Address</label>
              <textarea class="form-control wow fadeInUp txtarea" placeholder="Address"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Father's Name</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Father Name">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Father's Occupation</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Father Occupation">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Mother's Name</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Mother Name">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Mother's Occupation</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Mother Occupation">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Course Apply For</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Course --</option>
                <option value="Digital">Digital Mareketing</option>
                <option value="SEO">SEO</option>
                <option value="DESIGN">DESIGN</option>
              </select>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Post Graduation</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Stream</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Stream --</option>
                <option value="CSE">CSE</option>
                <option value="MECHANICAL">MECHANICAL</option>
                <option value="EEE">EEE</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">University/College</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your College">
            </p>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-1 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="50%">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Graduation</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Stream</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Stream --</option>
                <option value="CSE">CSE</option>
                <option value="MECHANICAL">MECHANICAL</option>
                <option value="EEE">EEE</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">University/College</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your College">
            </p>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-1 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="50%">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">PlusTwo</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Board</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Board --</option>
                <option value="Kerala">Kerala</option>
                <option value="CBSE">CBSE</option>
                <option value="NCRT">NCRT</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="50%">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">SSLC</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Board</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Board --</option>
                <option value="Kerala">Kerala</option>
                <option value="CBSE">CBSE</option>
                <option value="NCRT">NCRT</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="50%">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Upload Profile Image</label>
            </p>
            <div class="login-form-area ">
              <div class="file-upload wow fadeInUp">
                <div class="file-select">
                  <div class="file-select-button" id="fileName">Choose File</div>
                  <div class="file-select-name" id="noFile">No file chosen...</div>
                  <input type="file" name="chooseFile" id="chooseFile">
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Upload Signature</label>
            <div class="login-form-area ">
              <div class="file-upload wow fadeInUp">
                <div class="file-select">
                  <div class="file-select-button" id="fileName2">Choose File</div>
                  <div class="file-select-name" id="noFile2">No file chosen...</div>
                  <input type="file" name="chooseFile" id="chooseFile2">
                </div>
              </div>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Declaration</label>
              <textarea class="form-control wow fadeInUp txtarea" placeholder="Drop Your Words"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="login-form-area wow fadeInUp">
          <p>
            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
            <label for="styled-checkbox-1">I Agree</label>
          </p>
        </div>
      </div>
      <p>
        <button class="btn btn-default btn-primary wow fadeInUp" type="submit">SUBMIT</button>
      </p>
      <div class="login-form-register-now wow fadeInUp">
        <p>Already Registerd ? <a href="login.html">CLICK HERE</a></p>
      </div>
    </div>
  </form>
</div>

<!--Start Footer Area -->
<footer class="site-footer">
  <div class="footer-content">
    <div class="container wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">
      <h6>Suscribe our Newsletter</h6>
      <div class="newsletter">
        <form action="#" method="post">
          <input type="text" placeholder="Enter your Email" name="email">
          <button type="submit"><span class="send-icon"></span></button>
        </form>
      </div>
      <address>
      4th Floor, Markaz Complex, Mavoor Road,<br>
      Calicut, 673004,
      Email: info@passionleap.in,<br>
      Contact: +91 9744 642 225
      </address>
      <ul class="social-media">
        <li><a href="#"><img src="http://www.passionleap.in/assets/img/fb.png" alt=""></a></li>
        <li><a href="#"><img src="http://www.passionleap.in/assets/img/tw.png" alt=""></a></li>
        <li><a href="#"><img src="http://www.passionleap.in/assets/img/in.png" alt=""></a></li>
      </ul>
    </div>
  </div>
  <div class="copyright">
    <div class="container">
      <p>Copyright 2019 all rights reserved for Passion Leap</p>
    </div>
  </div>
</footer>
<!--End Footer Area --> 

<!-- All JS files are included here.-->
<script src="js/jquery-v3.2.1.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/wow.min.js"></script> 
<script src="js/dropdown-1.js"></script> 
<script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
<script>
jQuery(document).ready(function( $ ) {
  // Initiate the wowjs animation library
  new WOW().init();
  });
</script> 
<script>
   $('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
   $('#chooseFile2').bind('change', function () {
  var filename = $("#chooseFile2").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile2").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile2").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
</script> 
</body>
</html>