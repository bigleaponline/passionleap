<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->
    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one site-sectionevents">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s">
                        <h2>MEGA JOB FEST 2011</h2>
<p class="wow fadeInUp">No event is too small or irrelevant. We plan, design and execute each of the events keeping your future in mind.<br>
          Each event will benefit you in terms of building your career and personal growth. Get benefited.</p>
                    </div>
                </div>
            </div>
        </section>
<section class="gallery-sect">
    <div class="container">
      <div class="events-one">
        <div class="cont">
          <div class="demo-gallery wow fadeInUp">
            <ul id="lightgallery">
              <li data-responsive="assets/images/2011/2011-Job-Fair_Announcements.JPG" data-src="assets/images/2011/2011-Job-Fair_Announcements.JPG"
      
      data-sub-html="<h4>Job Fair_Announcements</h4><p>2011 Job Fair_Announcements</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_Announcements.JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="assets/images/2011/2011-Job-Fair_-Reg-Counter-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_-Reg-Counter-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_ Reg Counter</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_-Reg-Counter-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="assets/images/2011/2011-Job-Fair_BigLeap-MD-Mr.Soji-Abraham-giving-directions-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_BigLeap-MD-Mr.Soji-Abraham-giving-directions-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_BigLeap MD Mr.Soji Abraham giving directions</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_BigLeap-MD-Mr.Soji-Abraham-giving-directions-(Copy).JPG">
                </a> </li>
              <li data-responsive="assets/images/2011/2011-Job-Fair_Inaugural-Function-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_Inaugural-Function-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair_Inaugural Function</h4><p>2011 Job Fair_Inaugural Function</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_Inaugural-Function-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="assets/images/2011/2011-Job-Fair_Mayor-Arriving-for-Inauguration-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_Mayor-Arriving-for-Inauguration-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Mayor Arriving for Inauguration</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_Mayor-Arriving-for-Inauguration-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
              <li data-responsive="assets/images/2011/2011-Job-Fair_Registration-Candidates-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_Registration-Candidates-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Registration Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_Registration-Candidates-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="assets/images/2011/2011-Job-Fair_Candidates-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_Candidates-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Registration Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_Candidates-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
                
                   <li data-responsive="assets/images/2011/2011-Job-Fair_HCL-Counter-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_HCL-Counter-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Registration Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_HCL-Counter-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
                
                 <li data-responsive="assets/images/2011/2011-Job-Fair_Reg-Counter-(Copy).JPG" data-src="assets/images/2011/2011-Job-Fair_Reg-Counter-(Copy).JPG"
      data-sub-html="<h4>2011 Job Fair</h4><p>2011 Job Fair_Registration Candidates</p>" data-pinterest-text="Pin it" data-tweet-text="share on twitter "> <a href=""> <img class="img-responsive" src="assets/images/2011/2011-Job-Fair_Reg-Counter-(Copy).JPG">
                <div class="demo-gallery-poster"> <img src="assets/images/serchicon-event.png"> </div>
                </a> </li>
                
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
        
        <section class="clients clientscmn">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">PARTICIPATING <span style="color:#f25929">COMPANIES</span></h2>
      </div>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-06.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-07.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-08.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-09.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-10.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-11.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-12.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-13.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script type="application/javascript" src="<?php echo base_url();?>assets/js/lightgallery-all.js"></script> 
    <script type="application/javascript" src="<?php echo base_url();?>assets/js/mousewheel.js"></script>
    <script>
        new WOW().init();

    </script>
       <script>
        $(document).ready(function() {
	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    <script>
         jQuery(document).ready(function( $ ) {
           new WOW().init();
         });
         $(window).scroll(function(){
           var sticky = $('.sticky'),
               scroll = $(window).scrollTop();
           if (scroll >= 36) sticky.addClass('fixed');
           else sticky.removeClass('fixed');
         });
		$(document).ready(function() {
  $('#lightgallery').lightGallery({
    pager: true
  });
});
</script>
</body>

</html>
