
<?php
if($this->session->userdata['cand_data']['cand_test_status']==0)
{

?>
<!doctype html>
<?php include('include/header.php');?>

    <!-- Header End  -->
    <!-- Banner -->

    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one sect-intstruction">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s;">
                        <h2>Welcome <span><?php echo $this->session->userdata['cand_data']['name']; ?></span></h2>
                        <h3>Thank you for your registration </h3>
                        <h4> <i class="material-icons">list</i> general instruction for the assessment test</h4>
                        <!--<p>Our specialized RPO services are designed to effectively handle the entire recruitment process of our clients<br>
focusing on speed of delivery, quality of talent &amp; accountability. We are well equipped with talented consultants<br>
having varied industry knowledge &amp; expertise to execute complete recruitment assignments of the clients.</p>-->
<ul>
    <li><i class="material-icons">assignment_turned_in</i> The assessment test consist of four Aptitude tests.</li>
   <li><i class="material-icons">assignment_turned_in</i> The four types of tets are Verbal aptitude test , Quantitative aptitude test and Reasoning aptitude test.</li>
    <li><i class="material-icons">assignment_turned_in</i> You have to complete the four assessment test within the 30-minute time frame allotted for the exam.</li>
    <li><i class="material-icons">assignment_turned_in</i> The test consist of total of  30 multiple-choice-question with 10 questions in each each type type of aptitude tests </li>
    
    
</ul>
<?php 

      $attributes  = array('id' => 'myform'); 

      echo form_open() ?>
<button class="btn btn-default btn-primary wow fadeInUp"  onclick="goToAptitude(0)" >Ready For Assessment</button>
 <?php echo form_close(); ?>
						
                    </div>
                </div>
	

            </div>
        </section>
        
        
 
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
     <script src="<?php echo base_url();?>assets/js/aptitude_js.js"></script>
    <script>
        new WOW().init();

    </script>
     
       <!-- <script type="text/javascript">
            function preventBack() { window.history.forward(); }
            setTimeout("preventBack()", 0);
            window.onunload = function () { null };
        </script> -->
        <!-- <script type="text/javascript">
            
            function goToAptitude(){
//                 window.open ("aptitude",
// "_blank",params); 
// popup.moveTo(0,0)); 
//             }

var params = [
    'height='+screen.height,
    'width='+screen.width,
    'fullscreen=yes' // only works in IE, but here for completeness
].join(',');
var popup = window.open('aptitude', 'popup_window', params); 
popup.moveTo(0,0);

}
        </script>
     -->

</body>

</html>
<?php
    }
    else
    {
        redirect("logout");
    }

?>