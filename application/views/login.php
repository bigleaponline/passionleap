<!DOCTYPE html>

<?php include('include/header.php');?>

<div class="site-banner site-banner-service">
  <div class="container">
      
    <div class="banner-content wow fadeInUp" data-wow-delay="0.1s"> 
    <?php
if($this->session->flashdata('candidateregistration'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('candidateregistration').'</div>';
        }
        if($this->session->flashdata('profileregistration'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('profileregistration').'</div>';   
        }
        if($this->session->flashdata('approve'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('approve').'</div>';   
        }
        if($this->session->flashdata('loginerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('loginerror').'</div>';   
        }
?>

      <!--<span class="mouse-icon">
                    <div class="mouse-scroller"></div>
                </span>--> 
    </div>
  </div>
</div>
 
<!--Header Area End-->

<div class="register-page page-wrapper s-pd100">
   
  <div class="container">
    <div class="row">
      <div class="col-lg-7 col-md-7 col-sm-12">
        <div id="home-search-section wow fadeInUp" class="home-search-section-area bg-image home-header-one" style="background-image: url(assets/images/welcome-bg.jpg); background-size: cover;">
          <div class="container">
            <div class="row">
              <div class="col-lg-7 col-md-7 col-sm-12">
                <div class="welcome-text text-center tb">
                  <div class="tb-cell">
                    <h1 class="wow fadeInUp">Choose Your Extraordinary Future</h1>
                    <p class="wow fadeInUp">Explore your options, apply to Exams, Courses &amp; more</p>
                    <form class="product-search-form wow fadeInUp" action="#" method="get">
                      <button class="btn btn-default btn-primary" type="submit">Explore More </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-12">
        <div class="login-form-area">
          <form action="<?php echo base_url();?>passion_controller/login" method="post">
            <div class="text-center">
              <h2 class="section-heading text-capitalize wow fadeInUp">Login</h2>
            </div>
            <p>
            <?php
              if($this->session->flashdata('loginerror'))
              {
                echo '<div class="alert alert-warning" role="alert">'.$this->session->flashdata('loginerror').'</div>';   
              }
            ?>
            <input class="form-control wow fadeInUp" type="text" name="roll_no" id="roll_no" placeholder="Username">
            </p>
            <p>
              <input class="form-control wow fadeInUp" type="password" name="password" id="password" placeholder="Password">
            </p>
            <!--<a class="login-form-forgot-password wow fadeInUp" href="#">Forgot your password?</a>-->
            <p>
              <button class="btn btn-default btn-primary wow fadeInUp" type="submit">Log In Your Account</button>
            </p>
            <div class="login-form-remember wow fadeInUp">
              <label>
                <input type="checkbox" id="rememberme" value="">
                Keep me signed in until I sign out</label>
            </div>
            <div class="login-form-register-now wow fadeInUp">
              <p>Not yet a member? <a href="<?php echo base_url();?>register">REGISTER NOW</a></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="register-page-bottom"></div>

<!--Start Footer Area -->
<?php include('include/footer.php');?>
<!--End Footer Area --> 

<!-- All JS files are included here.
 jQuery Latest Version --> 
<script src="js/jquery-v3.2.1.min.js"></script> 
<!-- Bootstrap framework JS --> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/wow.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
<script>
jQuery(document).ready(function( $ ) {
  // Initiate the wowjs animation library
  new WOW().init();
  });
</script> 
<!--<script src="js/main.js"></script>-->
</body>
</html>