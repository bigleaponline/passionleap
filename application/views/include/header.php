<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Passion Leap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#f76448">
    <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon" sizes="16x16">
    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
   
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style-change.css">
    <!--<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">-->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/intlTelInput.css">
     <link rel="stylesheet" href="<?php echo base_url();?>assets/css/lightgallery.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158680874-2"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-v3.2.1.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script> 
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/js/dropdown-1.js"></script> 

 -->

 <script src = "http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>


    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
    
      gtag('config', 'UA-158680874-2');
    </script>

</head>

<body>
    <!-- Header -->
    <header class="site-header">
        <div class="container">
            <div class="logo-holder">
               <a href="http://www.passionleap.in/home"> <img src="<?php echo base_url();?>assets/img/logo.svg" alt="" /></a>
            </div>
            <div class="right-holder">
               <!-- <div class="search-bar">
                    <form>
                        <input type="text" placeholder="Search..">
                        <button type="submit"><span class="search-icon"></span></button>
                    </form>
                </div>-->
            <!--    <div class="main-menu">
                    <nav class="navbar navbar-expand-md navbar-dark">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="mainMenu">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>">Home</a></li>
                                <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>service">Services</a></li>
                                <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>register">Registration</a></li>
                                    <?php if($this->session->userdata('cand_data')){?>
                                     <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>logout">Logout</a></li><?php }else{?>
                                     <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>login">Login</a></li><?php }?>
                                    <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>contact">Contact</a>
                                    </li>
                            </ul>
                        </div>
                    </nav>
                </div>-->
                
            </div>
        </div>
    </header>
    
   <!-- <ul id="menu">
  <li class="current"> <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>">Home</a></li>
 
  <li><a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>service">Services</a></li>
  <li><a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>register">Registration</a></li>
  <li><a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>login">Login</a></li>
  <li><a href="#"></a></li>
</ul>
<a href="#menu" id="toggle">
  <span class="icon-bar"></span>
  <span class="icon-bar icon-right"></span>
  <span class="icon-bar icon-left"></span>
</a>-->



<header class="header">
  <div class="burger">
    <div class="burger__patty"></div>
    <div class="burger__patty"></div>
    <div class="burger__patty"></div>
  </div>

  <nav class="menu">
    <div class="menu__brand">
      <div class="logo"> <a href="http://www.passionleap.in/home"> <img src="<?php echo base_url();?>assets/img/logo.svg" alt="" /></a></div>
    </div>
    <ul class="menu__list">
      <li class="menu__item"><a class="nav-link hvr-underline-from-center menu__link" href="<?php echo base_url();?>home">Home</a></li>
      <li class="menu__item"><a class="nav-link hvr-underline-from-center menu__link" href="<?php echo base_url();?>service">Services</a></li>
       <li class="menu__item"> <a class="nav-link hvr-underline-from-center menu__link" href="<?php echo base_url();?>register">Registration</a></li>
         <?php if($this->session->userdata('cand_data'))
            {?>
         <li class="menu__item"> <a class="nav-link hvr-underline-from-center menu__link" href="<?php echo base_url();?>logout">Logout</a></li>
         <?php
            }
            else
            {
            ?>
         <li class="menu__item"> <a class="nav-link hvr-underline-from-center menu__link" href="<?php echo base_url();?>login">Login</a></li>
            <?php
            }
            ?>
      <li class="menu__item">
        <a href="https://www.facebook.com/pages/category/Career-Counselor/PassionLeap-100677361403520/" target="_blank" class="menu__link menu__link--social"><img src="http://www.passionleap.in/assets/img/fb.png" alt=""></a>
      </li>
      <li class="menu__item">
        <a href="https://www.instagram.com/_passionleap_/" target="_blank" class="menu__link menu__link--social">
         <img src="http://www.passionleap.in/assets/img/tw.png" alt=""></a>
      </li>
       <li class="menu__item">
        <a href="https://www.facebook.com/pages/category/Career-Counselor/PassionLeap-100677361403520/" target="_blank" class="menu__link menu__link--social"><img src="http://www.passionleap.in/assets/img/insta.png" alt=""></a>
      </li>
    </ul>
  </nav>
</header>


