<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-12 wow fadeInUp" data-wow-delay="0.1s">
                        <h2>CYBER SECURITY, ETHICAL HACKING</h2>
                        <p><b>CYBER SECURITY, ETHICAL HACKING Conducted Trainer and its's lucrative career paths</b><br>
The Basic Certificate in Cyber security and Ethical Hacking course is the most comprehensive course for the network security professionals. This globally acceptable certification authenticates the applied knowledge of the network administrators, auditors and professionals from a security perspective. A Certified Ethical Hacker Course will help you to think from the malicious hacker’s viewpoint but try to penetrate the network, ethically and list out the loop holes and vulnerabilities</p>
				
                    </div>
                    <div class="col-md-5 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder2">
                            <img src="<?php echo base_url();?>assets/img/service-03.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="services-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/service-04.png" alt="" />
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInUp">
                     <p><b>Mr. SANDEEP MUDALKAR (Cyber Crime Forensic Expert)</b><br>
Mr.Sandeep Mudalkar, Cyber Crime Forensic Expert, Investigator and Consultant to Hyderabad cyber cell, one of the pioneer cells in India that excels in bringing awareness in the cyber world about internet crime/fraud. He has been assisting Border Security Force, Gujarat Frontier, Hyderabad Cyber Cell, and Various Police Department across India besides being invited as Guest Lecturer in different colleges for training in Cyber Crime Investigation and Internet Security.
His expatiation includes Cyber Crime Investigation, Cyber Security, Internet Frauds, and Cyber Law Consulting. He is widely noted for his work as Cyber Crime Investigator and Consultant in Hyderabad Cyber Cell. He has also submitted his Research Papers in many of the Security Conferences. He runs Certified Training programmes on Advanced Level Professional Ethical Hacking, Cyber Crime Investigation, Cyber Forensics & Cyber Security at Hyderabad & various centres across India.</p>

                    </div>
                </div>
                <div class="row single-campaign">
                   <div class="col-md-4 wow fadeInLeft">
                       <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">Syllabus</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li><a href="#">Introduction to Ethical Hacking</a></li>
              <li><a href="#">Basics of networking and its concepts</a></li>
              <li><a href="#">Information Gathering (Foot printing and Reconnaissance)</a></li>
               <li><a href="#">Email Analysis, IP Grabbing, IP Tracking</a></li>
              <li><a href="#">Need of Email and IP Tracking</a></li>
               <li><a href="#">Scanning - finding target in network</a></li>
              <li><a href="#">IP Spoofing & MAC Spoofing and Proxy Server</a></li>
               <li><a href="#">Virus worms & Trojan and Sniffers</a></li>
              <li><a href="#">Phishing Attacks & Passwords cracking basic and security</a></li>
               <li><a href="#">DDoS attacks & Botnet & Malware and Introduction to Cryptog raphy & Steganography</a></li>
              <li><a href="#">Website Hacking & Malware Analysis</a></li>
               <li><a href="#">Mobile Hacking and Its security & Buffer overflow</a></li>
              <li><a href="#">Network Security, Fire wall, IDS & IPS and their log analysis & Registry Hacking</a></li>
              <li><a href="#">Introduction Vulnerability Assessment and Penetration Testing & Introduction to Kali Linux</a></li>
              <li><a href="#">SQL Mapping & Metasploit</a></li>
              <li><a href="#">Computer Forensic & Cyber Crime and Law</a></li>
            </ul>
          </div>
                        </div>
                         <div class="col-md-4 wow fadeInUp">
                             <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">Course Highlights</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- Most authentic and valid Govt.Certification (Skill India)</li>
              <li>- India’s most respected industry experts leading the fast track industry – academia bridge training program</li>
              <li>- In your campus based on mutual convenience with a minimum number of committed candidates</li>
              <li>- Free registration to India Mega Job Fairs for your final year students, events organized by Ministry of Labour & Employment, Model Career Center and SIGN. More than 30 companies to recruit</li>
              <li>- Government Certification</li>
            </ul>
             <h4 class="campaign-sidebar-title wow fadeInUp">TRAINING HIGHLIGHTS</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- Learn & Interact with renowned
                Industry Experts.</li>
              <li>- Receive an unparalleled education on the art of computer security with
                personal one-to-one attention.</li>
              <li>- Hands on Demonstrations of Latest Hacking Techniques & Tools.</li>
              <li>- Hands on Demonstrations of various cases solved by our Trainer.</li>
              <li>- All the necessary software would be provided.</li>
              <li>- PowerPoint Presentation, Live Demos, Interactive Question &answer sessions and comprehensive reading material.</li>
            </ul>
          </div>
                        </div>
                        
                <div class="col-md-4 col-12 mb-30 wow fadeInRight campaign-details-info">
                  
                    <img src="<?php echo base_url();?>assets/img/service-01.png" alt="" />
                   
               
                  <h5 class="wow fadeInUp">Service Details</h5>
                  <ul class="wow fadeInUp">
                    <li><span>Course:</span>CYBER SECURITY, ETHICAL HACKING Conducted Trainer AND IT’S LUCRATIVE CAREER PATHS</li>
                    <li><span>Conducted by Trainers</span>Mr. SANDEEP MUDALKAR</li>
                    <li><span>Event Categoried:</span> <a href="#">Candidates</a>, <a href="#">Presentations</a></li>
                    <li><span>Venue:</span>4th Floor, Markaz Complex, Mavoor Road, Calicut</li>
                  </ul>
                  <div class="campaign-sidebar">
             <h4 class="campaign-sidebar-title wow fadeInUp">SPECIALIZATIONS INCLUDES</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- Information Security Professional & Researcher</li>
              <li>- Vulnerability Research and Disclosure</li>
              <li>- Penetration Testing / Vulnerability Assessment of the
                Networks & Systems</li>
              <li>- Cyber Crime and Forensics Investigator</li>
            </ul>
          </div>
                   </div>
                        </div>
            </div>
        </section>
         <section class="course-slider">
              <div class="container">
                  
       <div class="popup-gallery">
           <div class="row">
           <div class="col-md-3 col-12 wow fadeInLeft">
    <a href="<?php echo base_url();?>assets/img/cybersecurity-braoucher-01.jpg" class="image" title="CYBER SECURITY, ETHICAL HACKING">
        <img src="<?php echo base_url();?>assets/img/cybersecurity-braoucher-01.jpg" alt="CYBER SECURITY, ETHICAL HACKING" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInUp">
    <a href="<?php echo base_url();?>assets/img/cybersecurity-braoucher-02.jpg" class="image" title="Mr. SANDEEP MUDALKAR
(Cyber Crime Forensic Expert)">
        <img src="<?php echo base_url();?>assets/img/cybersecurity-braoucher-02.jpg" alt="Mr. SANDEEP MUDALKAR
(Cyber Crime Forensic Expert)" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInUp">
    <a href="<?php echo base_url();?>assets/img/cybersecurity-braoucher-03.jpg" class="image" title="IT’S LUCRATIVE
CAREER PATHS">
        <img src="<?php echo base_url();?>assets/img/cybersecurity-braoucher-03.jpg" alt="IT’S LUCRATIVE
CAREER PATHS" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInRight">
     <a href="<?php echo base_url();?>assets/img/cybersecurity-braoucher-04.jpg" class="image" title="A GOVT OF INDIA
ENTERPRISE CERTIFIED
PROGRAM">
        <img src="<?php echo base_url();?>assets/img/cybersecurity-braoucher-04.jpg" alt="A GOVT OF INDIA
ENTERPRISE CERTIFIED
PROGRAM" style="" />
    </a>
      </div>
  </div>
   </div>
      </div>
        </section>
        <section class="clients clientscmn">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">PARTICIPATING <span style="color:#f25929">COMPANIES</span></h2>
      </div>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-06.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-07.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-08.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-09.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-10.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-11.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-12.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-13.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script>
        new WOW().init();

    </script>
       <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });


	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    <script>
        $('.popup-gallery').magnificPopup({
  delegate: 'a',
  type: 'image',
  gallery: {
    enabled: true,
    navigateByImgClick: true,
    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  },
  callbacks: {
    elementParse: function(item) {
      console.log(item.el[0].className);
      if(item.el[0].className == 'video') {
        item.type = 'iframe',
        item.iframe = {
           patterns: {
             youtube: {
               index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

               id: 'v=', // String that splits URL in a two parts, second part should be %id%
                // Or null - full URL will be returned
                // Or a function that should return %id%, for example:
                // id: function(url) { return 'parsed id'; } 

               src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
             },
             vimeo: {
               index: 'vimeo.com/',
               id: '/',
               src: '//player.vimeo.com/video/%id%?autoplay=1'
             },
             gmaps: {
               index: '//maps.google.',
               src: '%id%&output=embed'
             }
           }
        }
      } else {
         item.type = 'image',
         item.tLoading = 'Loading image #%curr%...',
         item.mainClass = 'mfp-img-mobile',
         item.image = {
           tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
         }
      }

    }
  }
});
    </script>
    
</body>

</html>
