<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner site-banner-change">
        <div class="container">
            <div class="banner-content wow fadeInUp" data-wow-delay="0.1s">
                <h4>Right Opportunities for </h4>
                <h2>the Right Talent</h2>
                <span class="mouse-icon">
                    <div class="mouse-scroller"></div>
                </span>
                <a href="http://www.passionleap.in/home" class="click-more">Go to Home</a>
            </div>
        </div>
        <p class="copyright-home wow fadeInUp">Copyright © 2020 all rights reserved for PassionLeap</p>
    </div>
    <!-- Footer -->
  
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lightgallery-all.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/mousewheel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/testi-slider.js"></script>
    <script>
        new WOW().init();
    </script>
    <script>
        $(document).ready(function() {
            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });
            var owl1 = $('#txtSlide');
            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });

$('#carousel01').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:false,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:2
                 },
                 1000:{
                     items:3
                 }
             }
});
	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    <script>
 jQuery(document).ready(function( $ ) {
   new WOW().init();
 });
 $(window).scroll(function(){
   var sticky = $('.sticky'),
	   scroll = $(window).scrollTop();
   if (scroll >= 36) sticky.addClass('fixed');
   else sticky.removeClass('fixed');
 });
$(document).ready(function() {
$('#lightgallery').lightGallery({
pager: true
});
});
</script>
</body>
</html>
