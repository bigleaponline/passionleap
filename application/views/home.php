<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
        <div class="container">
            <div class="banner-content wow fadeInUp" data-wow-delay="0.1s">
                <h4>Right Opportunities for </h4>
                <h2>the Right Talent</h2>
                <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one way or other but one may not be aware of what one can be.To create awareness in ones passion is our passion. PassionLeap helps the candidates to follow their passion by getting trained in various professional courses,which have yielded many a candidate to carve a niche in their domains of passion. Our professional courses are framed based on expert research to cater the basic foundation of the candidate and to build the efficiency of the candidate at various levels of advancement to engross professional outlook and compete in the contemporary opportunities. </p>
                <span class="mouse-icon">
                    <div class="mouse-scroller"></div>
                </span>
            </div>
        </div>
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                 <?php
                    if($this->session->flashdata('newsletter'))
                    {
                      echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('newsletter').'</div>';   
                    }
                    if($this->session->flashdata('newslettererror'))
                    {
                      echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('newslettererror').'</div>';   
                    }
                    ?>
                <div class="row">
                    <div class="col-md-5 col-sm-6 span3 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/future.png" alt="" />
                        </div>
                       
                    </div>
                     
                    <div class="col-md-7 wow fadeInUp" data-wow-delay="0.3s">
                        <h4>Passionate About</h4>
                        <h2>Creating Future</h2>
                        <p>Passion Leap is a movement to raise the spirits of young and dynamic youth in Kerala who aim for a passionate yet successful life ahead. In the present days, when Academics has become only a mere eligibility and a qualification to compete for career building, the responsibility lies with the candidate to carve the career independently, sooner or later upon achieving a qualification of their choice. To make the candidate ready to leap into a better career prospects is the objective of PassionLeap. Come, join our professional courses where your passion lies. Because, to build your career is our passion.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="site-section section-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-6 wow fadeInUp" data-wow-delay="0.1s"><br><br>
                        <h2>About <span style="color:#0a0808">Passion Leap</span></h2>
                        <p>Passion Leap is a movement to raise the spirits of young and dynamic youth in Kerala who aim for a passionate yet successful life ahead. In the present days, when Academics has become only a mere eligibility and a qualification to compete for career building, the responsibility lies with the candidateto carve the career independently, sooner or later upon achieving a qualification of their choice. To make the candidate ready to leap into a better career prospects is the objective of PassionLeap. Come, join our professional courses where your passion lies. Because, to build your career is our passion.</p>
                    </div>
                    <div class="col-md-5 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/about.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
 
        <section class="services-area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12 col-md-12" data-wow-delay="0.3s">
                        <div class="section-title text-center wow fadeInUp">
                            <h2 class="wow fadeInUp">OUR <span style="color:#f25929">COURSES</span> INCLUDED </h2>
                            <p class=" wow fadeInUp">PassionLeap offers an array of enriching courses specially designed by academic and <br>
            industry experts together to offer students the best of academic and industry experience.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-4" data-wow-delay="0.4s">
                        <div class="single-serviecs-block wow fadeInUp">
                            
                              <i class="material-icons">assignment</i>
                            <h3><a href="<?php echo base_url();?>icep">icep</a></h3>
                            <p>Career Enrichment Program (ICEP), is the combination of online
                  videos, practice exams, assessments and mock interviews.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-4" data-wow-delay="0.5s">
                        <div class="single-serviecs-block wow fadeInLeft">
                             <i class="material-icons">important_devices</i>
                            <h3><a href="<?php echo base_url();?>capitalmarket">capitalmarket</a></h3>
                            
                            
                            <p>A capital market may be a financial market during which long-term or equity-backed securities are bought and sold.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-4" data-wow-delay="0.6s">
                        <div class="single-serviecs-block wow fadeInRight">
                            
                             <i class="material-icons">speaker_notes</i>
                            <h3><a href="<?php echo base_url();?>cybersecurity">cybersecurity</a></h3>
                            <p>It refers to the body of technologies, processes, and practices designed to guard data from attack or unauthorized access.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="site-section section-three">
            <div class="container">
                <div class="slider-holder wow fadeInUp" data-wow-delay="0.1s">

                    <div class="owl-carousel owl-theme" id="txtSlide">
                        <div class="item">
                            <h2>Have you met your <span>Passion</span></h2>
                        </div>

                        <div class="item">
                            <h2>How to Identify your<span>Passion</span></h2>
                        </div>

                        <div class="item">
                            <h2>How to turn your Passion into <br><span>Profession</span></h2>
                        </div>

                        <div class="item">
                            <h2>Discover your Passion <span>now</span></h2>
                        </div>

                        <div class="item">
                            <h2>Share Your <span>Passion</span></h2>
                        </div>
                    </div>

                    <div class="owl-carousel owl-carousel-sub" id="mainSlide">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide1.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>Being Clever is to be clear<br> in identifying
                                        <span>Your Passion</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>


                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide2.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>Don’t go for a job just because <br>someone saidits good for
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide3.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>Dreams Really do come true.. <br>when passion awakes inside of
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide4.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>Your Energy in Action is the <br>Passion in
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide5.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>Follow your Passion and Success <br>will follow
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide6.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>Aptitude + Interest = <span>PASSION</span><br> Energy + Action = <span>SUCCESS</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide7.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>
                                        Allow your Passion, to become your purpose, and it will one day become your <span>profession</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide8.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>


                                        Check your Passion <span>Grit</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                    <a href="#">Take a Test</a>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide10.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>
                                        Explore <span>Courses</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>


                        <div class="item">
                            <div class="row">
                                <div class="col-md-5 col-sm-6" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide9.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-6" data-animation-in="fadeInRight">
                                    <h4>
                                        Register<span>Now</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

        </section>
        
   <div class="" style="clear: both;"></div>
        <section class="events wow fadeInUp">
    <div class="container">
      <div class="section_title text-center">
        <h2 class="wow fadeInUp">PASSIONLEAP <span style="color:#f25929">TRACK</span> RECORDS </h2>
                <p class="wow fadeInUp">No event is too small or irrelevant. We plan, design and execute each of the events keeping your future in mind.<br>
          Each event will benefit you in terms of building your career and personal growth. Get benefited.</p>
      </div>
      <div class="order-div" id="main-order">
        <div class="row">
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="9-div">
                 <a href="<?php echo base_url();?>jobfair-2020"><img src="<?php echo base_url();?>assets/images/folder-09.png" class="img-responsive"></a>
              <h3>2019 - 2020</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="8-div">
                <a href="<?php echo base_url();?>jobfair-2018"><img src="<?php echo base_url();?>assets/images/folder-08.png" class="img-responsive"></a>
              <h3>2017 - 2018</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="7-div">
                <a href="<?php echo base_url();?>jobfair-2015"><img src="<?php echo base_url();?>assets/images/folder-07.png" class="img-responsive"></a>
              <h3>2014 - 2015</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="6-div">
                <a href="<?php echo base_url();?>jobfair-2014"><img src="<?php echo base_url();?>assets/images/folder-06.png" class="img-responsive"></a>
              <h3>2013 - 2014</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="5-div">
                <a href="<?php echo base_url();?>jobfair-2013"><img src="<?php echo base_url();?>assets/images/folder-05.png" class="img-responsive"></a>
              <h3>2012 - 2013</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="4-div">
                <a href="<?php echo base_url();?>jobfair-2011"><img src="<?php echo base_url();?>assets/images/folder-04.png" class="img-responsive"></a>
              <h3>2010 - 20011</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="3-div">
               <a href="<?php echo base_url();?>jobfair-2006"><img src="<?php echo base_url();?>assets/images/folder-03.png" class="img-responsive"></a>
              <h3>2005 - 2006</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="2-div">
                <a href="<?php echo base_url();?>jobfair-2005"><img src="<?php echo base_url();?>assets/images/folder-02.png" class="img-responsive"></a>
              <h3>2004 - 2005</h3>
            </div>
          </div>
          <div class="col-md-3 col-sm-6 col-lg-3 col-xs-6">
            <div class="folder-link wow fadeInUp" id="1-div">
                <a href="<?php echo base_url();?>jobfair-2004"><img src="<?php echo base_url();?>assets/images/folder-01.png" class="img-responsive"></a>
              <h3>2003 - 2004</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="clear"></div>
        <section class="testimonials-sect wow fadeInUp">
    <div class="container">
      <div class="section_title text-center">
        <h2 class="wow fadeInUp">A FEW OF OUR <span style="color:#f25929">TALENTED</span>TRAINERS</h2>
        <p class="wow fadeInUp">A strategic thinker possesses strong business acumen and has a good grasp of how the training process directly affects the business. <br>
          Strategic thinking is not only restricted to being a part of training manager skills but is also one of the attributes of a good trainer as well.</p>
      </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <div class="owl-carousel feedback-slider wow fadeInUp" id="carousel03">
            <div class="feedback-slider-item"> <img src="assets/images/client-one.jpg" class="center-block img-circle img-responsive" alt="Customer Feedback">
              <h3 class="customer-name">mr. g Vijay Raghavan</h3>
              <p>Mr. G Vijay Raghavan is an MBA holder with more than a decade of experiance in the finance industry and has been responsible for the roles of Equity Advisor, Financial Counselling and Modelling of Portfolio.</p>
              <span class="light-bg customer-rating" data-rating="5"> 5 
                <i class="material-icons">star</i>
              </span> </div>
            <div class="feedback-slider-item"> <img src="assets/images/client-two.jpg" class="center-block img-circle img-responsive" alt="Customer Feedback">
              <h3 class="customer-name">MR. SANDEEP MUDALKAR</h3>
              <p>Mr. Sanddep Mudalkar, Cyber Crime Forensic Expert,Investigatar and Consultant to Hyderbad Cyber Cell, one of the poineer cells in india that excels in bringing.</p>
              <span class="light-bg customer-rating" data-rating="5"> 5  <i class="material-icons">star</i> </span> </div>
              
            <div class="feedback-slider-item"> <img src="assets/images/client-three.jpg" class="center-block img-circle img-responsive" alt="Customer Feedback">
              <h3 class="customer-name">Mr. Pramod Mullagiri</h3>
              <p>Mr. Pramod Mullagiri is an MBA professional with fourteen years of industry experience in Aerospace and IT. Have cultivated skills across the spectrum of Production and Consulting particularly in the areas of Production Planning, Scheduling and Control, Outsourcing, Customer Relationship and Service delivery.</p>
              <span class="light-bg customer-rating" data-rating="5"> 5  <i class="material-icons">star</i> </span> </div>
              
            <div class="feedback-slider-item"> <img src="assets/images/client-four.jpg" class="center-block img-circle img-responsive" alt="Customer Feedback">
              <h3 class="customer-name">burli saraswathi</h3>
              <p>Ms. Burli Saraswathi is an B-tech Professional with 5 year of Experience in IT nd Management.Currently Working as Marketing head and spearheading the marketing strategies by generating revenue through substantial leads.</p>
              <span class="light-bg customer-rating" data-rating="5"> 5  <i class="material-icons">star</i> </span> </div>
              
          </div>
          <div class="feedback-slider-thumb hidden-xs">
            <div class="thumb-prev wow fadeInUp"> <span> <img src="assets/images/client-three.jpg" alt="Customer Feedback" class="img-responsive"> </span> <span class="light-bg customer-rating"> 5<i class="material-icons">star</i> </span> </div>
            <div class="thumb-next wow fadeInUp"> <span> <img src="assets/images/client-two.jpg" alt="Customer Feedback" class="img-responsive"> </span> <span class="light-bg customer-rating"> 5 <i class="material-icons">star</i> </span> </div>
            <div class="thumb-next wow fadeInUp"> <span> <img src="assets/images/client-four.jpg" alt="Customer Feedback" class="img-responsive"> </span> <span class="light-bg customer-rating"> 5 <i class="material-icons">star</i> </span> </div>
          </div>
        </div>
      </div>
    </div>
  </section>
        <section class="dict-cnt">
    <div class="container">
      <div class="quick-cnt wow fadeInUp">
        <div class="row">
          <div class="col-md-9 col-sm-9">
            <h3 class="wow fadeInUp">Lets Get Started with PassinLeap Now</h3>
          </div>
          <div class="col-md-3 col-sm-3"> <a href="http://www.passionleap.in/login" class="quick-cnt-link wow fadeInUp">Creat Your Account</a> </div>
        </div>
      </div>
    </div>
  </section>
        <section class="clients">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">PARTICIPATING <span style="color:#f25929">COMPANIES</span></h2>
      </div>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-06.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-07.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-08.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-09.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-10.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-11.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-12.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-13.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>
        
    </div>
    <!-- Content End -->

    <!-- Footer -->
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/lightgallery-all.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/mousewheel.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/testi-slider.js"></script>
    <script>
        new WOW().init();
    </script>
    <script>
        $(document).ready(function() {
            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });
            var owl1 = $('#txtSlide');
            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });

$('#carousel01').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:false,
            responsive:{
                 0:{
                     items:1
                 },
                 600:{
                     items:2
                 },
                 1000:{
                     items:3
                 }
             }
});
	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    <script>
 jQuery(document).ready(function( $ ) {
   new WOW().init();
 });
 $(window).scroll(function(){
   var sticky = $('.sticky'),
	   scroll = $(window).scrollTop();
   if (scroll >= 36) sticky.addClass('fixed');
   else sticky.removeClass('fixed');
 });
$(document).ready(function() {
$('#lightgallery').lightGallery({
pager: true
});
});
</script>
</body>
</html>
