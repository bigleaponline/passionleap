<!doctype html>

<style type="text/css">
    .col-md-2.new, .col-md-5.new {

        
        margin-bottom: 20px;
        margin-left: 25px;
    }
body{

  font-family: Arial, Helvetica, sans-serif;
}
.head_tr{
}
.head_td{
}
}
.head_address{
  padding-left:20px;
  font-size: 12px;
 
  padding-top: 50px;
  width:250px;
  text-align: center;
}
.skills_td{
}
.progress-bar.new {
    background-color: #990099;
}

.progress-bar.new1{

  background-color: #428bca;
}

.progress-bar.new2{

  background-color: #d9534f;
}

.progress-bar.new3{

  background-color: #f0ad4e;
}

.progress-bar.new4{

  background-color: #5cb85c;
}


.col-md-6.skill_title {
    padding: 2% 0 2% 1%;
}

.col-md-6.skill_chart {
    padding: 2% 0 2% 0;
}
<?php
// print_r($english_score);exit;

      foreach($qualifcn as $qualification)
      {
        $qualification = $qualification['stream'];
      }
     if($english_score!=0){
      foreach ($english_score as $english_score) {

        $e_score = $english_score['total_score'];
        $e_percent = $e_score * 10;
      }
     }
      else
      {
          $e_score = 0;
        $e_percent = $e_score * 10;
      }
     if($quantitative_score!=0){
          foreach ($quantitative_score as $quantitative_score) {
    
            $q_score = $quantitative_score['total_score'];
            $q_percent = $q_score * 10;
          }
        }
      else
      {
          $q_score = 0;
        $q_percent = $q_score * 10;
      }
      
          
     
     if($reasoning_score!=0){ 
      foreach ($reasoning_score as $reasoning_score) {

        $r_score = $reasoning_score['total_score'];
        $r_percent = $r_score * 10;

      }
        }
      else
      {
        $r_score = 0;
        $r_percent = $r_score * 10;
      }
      
     if($verbal_score!=0){
      foreach ($verbal_score as $verbal_score) {

        $v_score = $verbal_score['total_score'];
        $v_percent = $v_score * 10;

      }
        }
      else
      {
          $v_score = 0;
        $v_percent = $v_score * 10;
      }
    ?>
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<?php include('include/header.php');?>
    <!-- Header End  -->
    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                <!--<div class="row">-->
                <!--    <div class="col-md-7 wow fadeInUp" data-wow-delay="0.1s">-->
                <!--        <h2>Assessment Report</h2>-->
                        
                <!--    </div>-->
                   
                <!--</div>-->
			

            </div>
        </section>
       
        
        <section class="clients clientscmn">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">Assessment <span style="color:#f25929">Report</span></h2>
      </div>
    <div class="container">
        
        
         <?php 

      $attributes  = array('id' => 'myform'); 

      echo form_open('report_pdf',$attributes) ?>
    <div align="center" class="pdf_btn" style="padding: 2%;">
      <input type="submit" name="cmd" id="cmd" value="Download Report">
      <!--<a href="<?php echo site_url("technical_report_print"); ?>" target="_blank" class="btn btn-info btn-xs" tooltip="Print" title="Print" data-toggle="modal"><i class="fa fa-print"></i></a>-->
    </div>
     <div class="col-md-12 col-xs-12">
      <div class="row">
       <div class="col-md-4 col-xs-4">
         <label><b>Name :</b> <?php echo $name;?></label>
       </div>
       <div class="col-md-4 col-xs-4">
         <label><b>Id :</b> <?php echo $cand_id;?></label>
       </div>
        <div class="col-md-4 col-xs-4">
         <label><b>Qualification :</b> <?php echo $qualification;?></label>
       </div>
       </div>
       <div class="row">
         <div class="col-md-6 col-xs-6 skill_title">
           <h6><b>Skill Distribution</b></h6>
         </div>
       </div>
       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Verbal Aptitude : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $e_percent;?>%;height: 15px;" class="progress-bar new1" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $e_score;?>/10</span>
       </div>
       
       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Quantitative Aptitude : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $q_percent;?>%;height: 15px;" class="progress-bar new2" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $q_score;?>/10</span>
       </div>

       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Reasoning Aptitude : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $r_percent;?>%;height: 15px;" class="progress-bar new3" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $r_score;?>/10</span>
       </div>

       <!--<div class="row">-->
       <!--  <div class="col-md-3 col-xs-3">-->
       <!--   <label>Verbal Aptitude : </label>-->
       <!--  </div>-->
       <!--  <div class="col-md-6 col-xs-6">-->
       <!--    <div data-percentage="0%" style="width: <?php echo $v_percent;?>%;height: 15px;" class="progress-bar new3" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>-->
       <!--  </div>-->
       <!--   <span><?php echo $v_score;?>/10</span>-->
       <!--</div>-->
       <div class="row">
         <div class="col-md-6 col-xs-6 skill_chart">
            <div id="piechart_3d" style="width: 800px; height: 500px;"></div>
            <input type="hidden" name="chrt_img" id="chrt_img">
         </div>
       </div>
     </div>
     <table class="header_tb" width="100%" style="height:auto;" ></table>
     <?php echo form_close(); ?>
        
        
        
      
    </div>
  </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script>
        new WOW().init();

    </script>
       <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });


	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    
 <script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Assessment', 'Marks per Topic'],
      ['Verbal',     <?php echo $e_score;?>],
      ['Quantitative ',     <?php echo $q_score;?>],
      ['Reasoning',      <?php echo $r_score;?>]
    ]);

    var options = {
      title: 'My Mark Distribution',
      is3D: true,
    };
    
    var chart_area = document.getElementById('piechart_3d');

    var chart = new google.visualization.PieChart(chart_area);

    // var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
    
    google.visualization.events.addListener(chart, 'ready', function(){
    chart_area.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';


   // var chart_pic = new google.visualization.PieChart(document.getElementById('chart_div'));
  // download(chart_pic.getImageURI(), 'fileName.png', "http://localhost:8080/thesmartsoft/upload/chartimages");

 // console.log(chart_area.innerHTML);

  //chart_input.value = chart.getImageURI();
  

  // document.getElementById('img_dwnld').href=chart.getImageURI();
  document.getElementById("chrt_img").value =chart.getImageURI();

    });    

    chart.draw(data, options);
  }
</script>   
    
</body>

</html>
