<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-12 wow fadeInUp" data-wow-delay="0.1s">
                        <h2>INTEGRATED CAREER ENRICHMENT PROGRAM (ICEP)</h2>
                        <p>ICEP is the Govt. Of India Certified Program, Most companies especially MNC giants; gauge a candidate’s overall efficiency by keeping the “7 skills” as the cornerstones of assessment. Last year ,a study by employabilitty assessment company Aspiring Minds created a stir by claiming that 95 percent of engineers in the country were not fit for sotware development jobs. "The top 10 IT Companies take only 6% of the engineering graduates.what happends to the remaining 94%" ? Due to the widening skill gap, now industry has to re-train even those get hired. Moreover indistry needs packaged students who are good at aptittude, technical skills and also soft skills. hence the above said students will not be alibible to crack a job and falter in the process. some of the students either overestimate or understatimate their skills think and net result is that they do not concentrate on tracking either assessment.</p>
					<P>Integrated Career Enrichment Program (ICEP), is the Compintion of online videos, practice exams, assessment and mock interviews. so ICEP help the students to find out their weaker area and provide chance to perform well in real time selection process.</P>
				
                    </div>
                    <div class="col-md-5 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/about.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="services-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/service-04.png" alt="" />
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInUp">
                     <P>Integrated Career Enrichment Program (ICEP) is a platform which believes in creating a process flow for students to get succeeded in their career efforts and crack thier first job.

HOW TO GET INTO TOP MNC’S? CRACK ANY JOB INTERVIEWS BY ATTENDING 7 SKILLS TRAINING
A person’s collective ability to adapt to an environment, react to a situation, think, comprehend, calculate and act is mostly down to the “7 SKILLS” present in them.
If a candidate has in him/her instilled the right quantity and quality of “7 SKILLS” the chances of securing the dream-job becomes greater than ever before. Here, we present a comprehensive program to work on all the seven aspects that determine the job-readiness of a candidate.
This program enables candidates to adapt themselves to face any specific competitive exams like TCS, Wipro, HCL, CTS, AMCAT, Co- Cubes, E- Litmus etc. by following a generic two tier training architecture and specific assessing models.</P>

                    </div>
                </div>
                <div class="row single-campaign">
                   <div class="col-md-4 wow fadeInLeft">
                       <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">Syllabus</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li><a href="#">Quantitative Aptitude</a></li>
              <li><a href="#">Verbal Aptitude</a></li>
              <li><a href="#">Reasoning</a></li>
              <li><a href="#">Introduction Python
                Programming</a></li>
              <li><a href="#">Soft Skill Training</a></li>
              <li><a href="#">Group Discussion</a></li>
              <li><a href="#">Public Speaking</a></li>
              <li><a href="#">Debate &amp; Topic
                Presentation</a></li>
              <li><a href="#">Interview training &amp;
                Motivation</a></li>
              <li><a href="#">Personality Training &amp;
                Grooming</a></li>
              <li><a href="#">Mock Interview</a></li>
            </ul>
          </div>
                        </div>
                         <div class="col-md-4 wow fadeInUp">
                             <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">Course Highlights</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- Most authentic and valid Govt.Certification (Skill India) </li>
              <li>- India’s most respected industry experts leading the fast track industry – academia bridge training program</li>
              <li>- In your campus based on mutual convenience with a minimum number of committed candidates</li>
              <li>- Free registration to India Mega Job Fairs for your final year students, events organized by Ministry of Labour & Employment, Model Career Center and SIGN. More than 30 companies to recruit</li>
              <li>- Government Certification</li>
              
            </ul>
          </div>
                        </div>
                        
                <div class="col-md-4 col-12 mb-30 wow fadeInRight campaign-details-info">
                  
                    <img src="<?php echo base_url();?>assets/img/service-01.png" alt="" />
                   
               
                  <h5 class="wow fadeInUp">Service Details</h5>
                  <ul class="wow fadeInUp">
                    <li><span>Course:</span>Integrated Career Enrichment Program (ICEP)</li>
                    <li><span>Conducted by Trainers</span>Mr.PRAMOD MULLAGIRI &amp; <br>
                      Mrs.BURLI SARASWATHI</li>
                    <li><span>Event Categoried:</span> <a href="#">Candidates</a>, <a href="#">Presentations</a></li>
                    <li><span>Venue:</span>4th Floor, Markaz Complex, Mavoor Road, Calicut</li>
                  </ul>
                   </div>
                        </div>
            </div>
        </section>
         <section class="course-slider">
              <div class="container">
                  
       <div class="popup-gallery">
           <div class="row">
           <div class="col-md-3 col-12 wow fadeInLeft">
    <a href="<?php echo base_url();?>assets/img/icep-broucher-01.jpg" class="image" title="CAMPUS PLACEMENT READINESS PROGRAM">
        <img src="<?php echo base_url();?>assets/img/icep-broucher-01.jpg" alt="Alt text" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInUp">
    <a href="<?php echo base_url();?>assets/img/icep-broucher-02.jpeg" class="image" title="Hon’ble Minister for Environment, Forest and Climate Change Mr. Babul Supriyo inaugurating Mega Job Fair at Thissur 02-02-2020">
        <img src="<?php echo base_url();?>assets/img/icep-broucher-02.jpeg" alt="CAMPUS PLACEMENT READINESS PROGRAM" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInUp">
    <a href="<?php echo base_url();?>assets/img/icep-broucher-03.jpg" class="image" title="CAMPUS PLACEMENT READINESS PROGRAM">
        <img src="<?php echo base_url();?>assets/img/icep-broucher-03.jpg" alt="CAMPUS PLACEMENT READINESS PROGRAM" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInRight">
     <a href="<?php echo base_url();?>assets/img/icep-broucher-04.jpg" class="image" title="CAMPUS PLACEMENT READINESS PROGRAM">
        <img src="<?php echo base_url();?>assets/img/icep-broucher-04.jpg" alt="CAMPUS PLACEMENT READINESS PROGRAM" style="" />
    </a>
      </div>
  </div>
   </div>
      </div>
        </section>
        <section class="clients clientscmn">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">PARTICIPATING <span style="color:#f25929">COMPANIES</span></h2>
      </div>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-06.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-07.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-08.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-09.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-10.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-11.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-12.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-13.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script>
        new WOW().init();

    </script>
       <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });


	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    <script>
        $('.popup-gallery').magnificPopup({
  delegate: 'a',
  type: 'image',
  gallery: {
    enabled: true,
    navigateByImgClick: true,
    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  },
  callbacks: {
    elementParse: function(item) {
      console.log(item.el[0].className);
      if(item.el[0].className == 'video') {
        item.type = 'iframe',
        item.iframe = {
           patterns: {
             youtube: {
               index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

               id: 'v=', // String that splits URL in a two parts, second part should be %id%
                // Or null - full URL will be returned
                // Or a function that should return %id%, for example:
                // id: function(url) { return 'parsed id'; } 

               src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
             },
             vimeo: {
               index: 'vimeo.com/',
               id: '/',
               src: '//player.vimeo.com/video/%id%?autoplay=1'
             },
             gmaps: {
               index: '//maps.google.',
               src: '%id%&output=embed'
             }
           }
        }
      } else {
         item.type = 'image',
         item.tLoading = 'Loading image #%curr%...',
         item.mainClass = 'mfp-img-mobile',
         item.image = {
           tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
         }
      }

    }
  }
});
    </script>
    
</body>

</html>
