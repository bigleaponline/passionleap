<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-12 wow fadeInUp" data-wow-delay="0.1s">
                        <h2>CAPITAL MARKET</h2>
                        <p>When most people tell you about their plans of pursuing a career in finance, capital market, most often it is understood that they will be working for a bank. That could be the case years ago; however, there are limitless opportunities and rewarding career choices for individuals from all backgrounds, irrespective of their gender, age, educational qualification to an extent, and interest. Moreover, due to liberalization, the Indian economy has given rise to the opportunities in the capital markets for national and international players. Hence there are strong career choices for anyone planning to pursue a future in capital markets. To be sure of your career in the capital markets, one needs to first get clarity of the term. This course is a perfectly designed course, to create a practical understanding and approach to capital markets and fundamental research analysis of a stock market.</p>
				
                    </div>
                    <div class="col-md-5 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder2">
                            <img src="<?php echo base_url();?>assets/img/service-07.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="services-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 wow fadeInLeft">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/service-04.png" alt="" />
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInUp">
                     <P>LEARN TO EARN FROM CAPITAL MARKET - Trainer Details</P>
                     <p>Mr.G VIJAY RAGHAVAN (Head of Pentad Securities)
Mr. G Vijay Raghavan is an MBA holder with more than decade of experience in the finance industry and has been responsible for the roles of Equity Advisor, Financial Counselling and modelling of portfolio. Later he shifted to sales sector to understand the consumer behaviour on financial product. Currently, he holds the responsibility of National Head at Pentad Securities.
He has guided over 24600 professionals in last 10 years of his career. He started his career as an intern at Reliance Money and has been interested in guiding students from the beginning as it’s very much necessary for every student to understand the practical aspect. He is an MBA Graduate from Jain University.</p>

                    </div>
                </div>
                <div class="row single-campaign">
                   <div class="col-md-4 wow fadeInLeft">
                       <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">Syllabus</h4>
            <h5 class="campaign-sidebar-title wow fadeInUp">Equity instruments</h5>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li><a href="#">IPO valuation technique
                as an investor</a></li>
              <li><a href="#">Cash and derivatives
                segment</a></li>
              <li><a href="#">Placing of orders and
                monitoring process</a></li>
            </ul>
            <h5 class="campaign-sidebar-title wow fadeInUp">Fundamental Approach</h5> 
             <ul class="campaign-sidebar-links wow fadeInUp">
              <li><a href="#">How to read an annual report</a></li>
              <li><a href="#">Comparative analysis with quarterly result live</a></li>
              <li><a href="#">20:50 Rule book</a></li>
              <li><a href="#">How to implement in decision making</a></li>
              <li><a href="#">Corporate governance monitoring</a></li>
              <li><a href="#">How to create your own portfolio</a></li>
            </ul>
            <h5 class="campaign-sidebar-title wow fadeInUp">Practical Session</h5> 
             <ul class="campaign-sidebar-links wow fadeInUp">
              <li><a href="#">Live market monitoring
                Optimization (SEO)</a></li>
              <li><a href="#">Mock stock tutorial</a></li>
              <li><a href="#">Value based stock
                picking</a></li>
              <li><a href="#">Sectoral analysis</a></li>
              <li><a href="#">Practical approach in
                order placing processes</a></li>
              <li><a href="#">Portfolio management</a></li>
              <li><a href="#">Regular evaluation of
                your portfolio –
                techniques</a></li>
              <li><a href="#">Trade Carnival (Live)</a></li>
            </ul>
          </div>
                        </div>
                         <div class="col-md-4 wow fadeInUp">
                             <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">Course Highlights</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- Most authentic and valid Govt.Certification (Skill India)</li>
              <li>- India’s most respected industry experts leading the fast track industry – academia bridge training program</li>
              <li>- In your campus based on mutual convenience with a minimum number of committed candidates</li>
              <li>- Free registration to India Mega Job Fairs for your final year students, events organized by Ministry of Labour & Employment, Model Career Center and SIGN. More than 30 companies to recruit</li>
              <li>- Government Certification</li>
            </ul>
             <h4 class="campaign-sidebar-title wow fadeInUp">BENEFITS</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- Become Entrepreneur</li>
              <li>- Career opportunity as Equity Advisor Most authentic and valid Govt</li>
              <li>- India’s most respected industry experts leading the fast track industry – academia bridge training program</li>
              <li>- Free registration to India Mega Job Fairs for your final year students, Events organized by Ministry of Labour & Employment, Model career centre and SIGN. More than 30 companies to recruit.</li>
              <li>- Government Certification</li>
            </ul>
          </div>
                        </div>
                        
                <div class="col-md-4 col-12 mb-30 wow fadeInRight campaign-details-info">
                  
                    <img src="<?php echo base_url();?>assets/img/service-01.png" alt="" />
                   
               
                  <h5 class="wow fadeInUp">Service Details</h5>
                  <ul class="wow fadeInUp">
                    <li><span>Course:</span>LEARN TO EARN FROM CAPITAL MARKET</li>
                    <li><span>Conducted by Trainers</span>Mr.G VIJAY RAGHAVAN</li>
                    <li><span>Event Categoried:</span> <a href="#">Candidates</a>, <a href="#">Presentations</a></li>
                    <li><span>Venue:</span>4th Floor, Markaz Complex, Mavoor Road, Calicut</li>
                  </ul>
                  <div class="campaign-sidebar">
            <h4 class="campaign-sidebar-title wow fadeInUp">OBJECTIVES</h4>
            <ul class="campaign-sidebar-links wow fadeInUp">
              <li>- KNOWLEDGE TRANSFORMATION -</li>
              <li>* To achieve students to understand crux of stock market.</li>
              <li>- INDUSTRY EXPERTS -</li>
              <li>* Sharing experiences by industry experts.</li>
              <li>- PRACTICAL AWARENESS -</li>
              <li>* Quizzes and Games including hands on experience on live trading</li>
              <li>- TECHNOLOGY IMPLEMENTATION -</li>
              <li>* We help you to Implement Technology in a new method.</li>
            </ul>
          </div>
                   </div>
                        </div>
            </div>
        </section>
         <section class="course-slider">
              <div class="container">
                  
       <div class="popup-gallery">
           <div class="row">
           <div class="col-md-3 col-12 wow fadeInLeft">
    <a href="<?php echo base_url();?>assets/img/capital-marketing-broucher-01.jpg" class="image" title="CAPITAL MARKET">
        <img src="<?php echo base_url();?>assets/img/capital-marketing-broucher-01.jpg" alt="CAPITAL MARKET" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInUp">
    <a href="<?php echo base_url();?>assets/img/capital-marketing-broucher-02.jpg" class="image" title="LEARN TO EARN FROM CAPITAL MARKET">
        <img src="<?php echo base_url();?>assets/img/capital-marketing-broucher-02.jpg" alt="LEARN TO EARN FROM CAPITAL MARKET" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInUp">
    <a href="<?php echo base_url();?>assets/img/capital-marketing-broucher-03.jpg" class="image" title="A GOVT OF INDIA
ENTERPRISE CERTIFIED
PROGRAM">
        <img src="<?php echo base_url();?>assets/img/capital-marketing-broucher-03.jpg" alt="A GOVT OF INDIA
ENTERPRISE CERTIFIED
PROGRAM" style="" />
    </a>
</div>
<div class="col-md-3 col-12 wow fadeInRight">
     <a href="<?php echo base_url();?>assets/img/capital-marketing-broucher-04.jpg" class="image" title="A GOVT OF INDIA
ENTERPRISE CERTIFIED
PROGRAM">
        <img src="<?php echo base_url();?>assets/img/capital-marketing-broucher-04.jpg" alt="A GOVT OF INDIA
ENTERPRISE CERTIFIED
PROGRAM" style="" />
    </a>
      </div>
  </div>
   </div>
      </div>
        </section>
        <section class="clients clientscmn">
    <div class="section_title text-center">
      <h2 class="wow fadeInUp">PARTICIPATING <span style="color:#f25929">COMPANIES</span></h2>
      </div>
    <div class="container">
      <div class="owl-carousel owl-theme mobilesliderpro" id="carousel05">
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-02.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-03.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-04.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-05.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-06.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-07.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-08.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-09.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-10.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-11.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-12.png" class="img-responsive"></a></div>
        <div class="item wow fadeInUp"><a href="#"><img src="assets/images/clients-13.png" class="img-responsive"></a></div>
      </div>
    </div>
  </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/toggle-menu.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script>
        new WOW().init();

    </script>
       <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });


	$('#carousel05').owlCarousel({
            nav:true,
         	margin:20,
         	loop:true,
			autoplayTimeout:5000,
         	mouseDrag:true,
    		autoplay:true,
            responsive:{
                 0:{
                     items:2
                 },
                 600:{
                     items:3
                 },
                 1000:{
                     items:6
                 }
             }
});

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
    <script>
        $('.popup-gallery').magnificPopup({
  delegate: 'a',
  type: 'image',
  gallery: {
    enabled: true,
    navigateByImgClick: true,
    preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  },
  callbacks: {
    elementParse: function(item) {
      console.log(item.el[0].className);
      if(item.el[0].className == 'video') {
        item.type = 'iframe',
        item.iframe = {
           patterns: {
             youtube: {
               index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

               id: 'v=', // String that splits URL in a two parts, second part should be %id%
                // Or null - full URL will be returned
                // Or a function that should return %id%, for example:
                // id: function(url) { return 'parsed id'; } 

               src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
             },
             vimeo: {
               index: 'vimeo.com/',
               id: '/',
               src: '//player.vimeo.com/video/%id%?autoplay=1'
             },
             gmaps: {
               index: '//maps.google.',
               src: '%id%&output=embed'
             }
           }
        }
      } else {
         item.type = 'image',
         item.tLoading = 'Loading image #%curr%...',
         item.mainClass = 'mfp-img-mobile',
         item.image = {
           tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
         }
      }

    }
  }
});
    </script>
    
</body>

</html>
