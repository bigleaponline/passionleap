<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passion_controller extends CI_Controller 
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('passion_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('test_helper');
        
        
        //$this->load->library('pdf');
    }
    public function index()
    {
        $this->load->view('index');
    }
    public function loadPages()
    {
        $page=$this->uri->segment('1');
        $this->load->view($page);
    }
    public function register()
    {
       $data['cand_id']=$this->passion_model->get_id(); 
       $this->load->view('register',$data);
    }
    public function candidate_Registration()
    {
        
        $cand_registration=$this->passion_model->candidate_Registration();
        if($cand_registration)
        {
            $this->session->set_flashdata('candidateregistration','You Are Registered Successfully...Password and Username is Send to your mail id !!');
            redirect('login');
        }
        else
        {
            $this->session->set_flashdata('candidatregistrationerror','Failed.... Please try again...');
            redirect('register');
        }
        
    }
    public function login()
    {
        $login=$this->passion_model->login();
        if($login)
        {
            
            $this->session->set_userdata('cand_data',$login);
            $status = $data['status'] = $this->session->userdata['cand_data']['status'];
            $cand_test_status = $data['cand_test_status'] = $this->session->userdata['cand_data']['cand_test_status'];
            $this->session->set_flashdata('login',"Login Successfully");
            
            if($status == 2 && $cand_test_status==0)
            {
                redirect('instruction');
            }
            if($status == 0 && $cand_test_status==0)
            {
            redirect('register_final');
            }
            if($status == 2 && $cand_test_status!=0)
            {
            redirect('report');
            }
        }
        else
		{
			redirect('login');
		}
    }
    public function newsLetter()
    {
        $newsletter=$this->passion_model->newsLetter();
        if($newsletter)
        {
            $this->session->set_flashdata('newsletter','You Are Subscribe Successfully');
            redirect(base_url());
        }
        else
        {
            $this->session->set_flashdata('$newslettererror','Failed.... Please try again...');
            redirect(base_url());
        }
    }
    // public function get_question_list($cand_level='',$topic_id='')
    // {
    //     $data['general_question'] = $this->passion_model->get_general_question();
            
    //     $this->load->view('aptitude',$data);   
    // }
    
     public function technical_assesment()
    {
        $cand_id = $this->session->userdata['cand_data']['cand_id'];
        // $status=$this->session->userdata['cand_data']['cand_test_status'];
        // $category=$this->session->userdata['cand_data']['cand_category'];
       // echo "fff";exit;
        $status= $this->passion_model->get_cand_status($cand_id);
        $data['status'] = $this->session->userdata['cand_data']['status']=$status;

        
                
        if($status==2)
        {
           $topic_id=$this->uri->segment('2'); 
            if($topic_id == "" || $topic_id==1)
            {
              $this->session->unset_userdata('exm_time');
               // $e=$this->session->set_userdata('exm_time',0);
                //echo "bbbbbbb".$e; exit();
                
                $topic_id  = 1;
                $cand_level= 1;
            }
            else
                $cand_level= 2;
            
                
           
            $data['cand_id'] = $this->session->userdata['cand_data']['cand_id'];
            $data['cand_level'] = $cand_level;
            $data['topic_id'] = $topic_id;
            $data['question_list'] = $this->passion_model->get_question_list($cand_level,$topic_id);
            $start_time = date('H:i:s');
            $data['page_title']   = $this->uri->segment('1');
            $this->session->set_userdata('start_time',$start_time);   
            
            $this->load->view('aptitude',$data);  
        }
        else
        {
            redirect('logout');
        }
    }
    
    
    public function assesment_test_submission()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        //$cand_category=$this->session->userdata['cand_data']['cand_category'];
        $exm_time_out=$this->input->post('exm_time_out');
        //if($this->session->userdata('exm_time'))
        $exm_time=$this->session->userdata('exm_time');
       // else
          //  $exm_time=0;
        
        $topic_id=$this->uri->segment('3');

        // echo  $topic_id."---".$exm_time_out;exit();
        
        if( $topic_id!=1)
        $cand_level=2;
        
        $end_time = date('H:i:s');
        $this->session->set_userdata('end_time',$end_time);
        $this->passion_model->save_test_answer();

        $test_status= $this->passion_model->get_cand_test_status($cand_id);
        $this->session->userdata['cand_data']['cand_test_status']=$test_status;


        
        if($topic_id == 1 && $exm_time_out==0)
        {
            redirect('technical_assesment/2');
        }
        elseif($topic_id == 2 && $exm_time_out==0)
        {
            redirect('technical_assesment/3');
        }
        // elseif ($topic_id == 3) 
        // {
        //   redirect('technical_assesment/4');
        // }
        else
        { 
           
             if($topic_id == 3 || $exm_time_out==1)
            {
                $this->session->unset_userdata('exm_time');
                redirect("report");
            }
        }
    }
    
    // public function technical_report()
    // {
    // echo "dhfiuh";
    // }
    
    public function technical_report()
    {
        echo "jvijsdj".exit;
        if($this->session->userdata('cand_data'))
        {
            //echo "hbdhsd";exit;
            $cand_id                    =$this->session->userdata['cand_data']['cand_id'];
            $report['cand_id']          = $cand_id;
            $cand_name                  =$this->session->userdata['cand_data']['name'];
            $report['name']             = $cand_name;
            $report['qualification']    = $this->passion_model->get_cand_qualification($cand_id);
           // $report['cand_level']       = $this->session->userdata['cand_data']['cand_level'];
            $report['quantitative_score']  = $this->passion_model->get_score($cand_id,2);
            $report['reasoning_score']     = $this->passion_model->get_score($cand_id,3);
            $report['verbal_score']        = $this->passion_model->get_score($cand_id,4);
            $data['page_title']            =$this->uri->segment('2');
            
            
            $this->load->view('technical_report',$report);
            
        }
        else
        {
            redirect('logout');
        }
    }
    
    
    public function technical_report_pdf()
    {
          $cand_id           =$this->session->userdata['cand_data']['cand_id'];
        $report['cand_id'] = $cand_id;
        $cand_name           =$this->session->userdata['cand_data']['name'];
        $report['name'] = $cand_name;
        $report['qualfcn'] = $this->passion_model->get_cand_qualification($cand_id);
        //$report['cand_level'] = $this->session->userdata['cand_data']['cand_level'];
        $report['chrt_img']       = $this->input->post('chrt_img');
        $report['english_score']  = $this->passion_model->get_score($cand_id,1);
        $report['quantitative_score']  = $this->passion_model->get_score($cand_id,2);
        $report['reasoning_score']  = $this->passion_model->get_score($cand_id,3);
        $report['verbal_score']  = $this->passion_model->get_score($cand_id,4);
        // $this->pdf->load_view('technical_report_pdf',$report);
        // $this->pdf->render();
        // $this->pdf->stream("technical_report_pdf.pdf");
        
        ob_end_clean();
        // $html = $this->load->view('user_list');
        $html = $this->load->view('technical_report_pdf',$report);
        $html = $this->output->get_output();
        $this->load->library('pdf');
        $this->dompdf->loadHtml($html);
        //$this->dompdf->setPaper('A4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("welcome.pdf", array("Attachment"=>0));
        
       // $this->pdf->createPDF($html, 'mypdf', false);
    }
    
    
    
    public function aptitude_submission()
    {
        $aptitude_result=$this->passion_model->save_aptitude_result();

        redirect('report');

    }
    public function report()
    {
    
        if($this->session->userdata('cand_data'))
        {
            //echo "hbdhsd";exit;
            $cand_id                    =$this->session->userdata['cand_data']['cand_id'];
            $report['cand_id']          = $cand_id;
            $cand_name                  =$this->session->userdata['cand_data']['name'];
            $report['name']             = $cand_name;
            $report['qualification']    = $this->passion_model->get_cand_qualification($cand_id);
            $report['qualifcn']  = $this->passion_model->get_cand_qualification($cand_id);
           // $report['cand_level']       = $this->session->userdata['cand_data']['cand_level'];
           
           // $english_score                 = $this->passion_model->get_score($cand_id,1);
            
           
            $report['english_score']       = $this->passion_model->get_score($cand_id,1);
            $report['quantitative_score']  = $this->passion_model->get_score($cand_id,2);
            $report['reasoning_score']     = $this->passion_model->get_score($cand_id,3);
            $report['verbal_score']        = $this->passion_model->get_score($cand_id,4);
            $data['page_title']            =$this->uri->segment('2');
            
            
            $this->load->view('technical_report',$report);
            
        }
        else
        {
            redirect('logout');
        }
       
       
       
       
       
       
       
        // $cand_id              = 1;
        // $report['result']     = $this->passion_model->get_report($cand_id);
        
        // if($report)
        // {
        //     $report['cand_id']    = $cand_id;            
           
        //     $this->load->view('report',$report);
            
        // }
        // else
        // {
        //     redirect('logout');
        // }
    }
    
    public function second_register()
    {

        $cand_id = $data['cand_id'] = $this->session->userdata['cand_data']['cand_id'];
        $name =  $data['name'] = $this->session->userdata['cand_data']['name'];
        $data['cand_details']=$this->passion_model->get_cand_details($cand_id); 
        $this->load->view('register_final', $data);
    }
    public function do_upload_profile_image() 
    {
        $config['upload_path'] = './upload/profile_images';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '300000';
        $config['max_width'] = '150';
        $config['max_height'] = '200';
        $config['overwrite'] = TRUE;
        $profileimage = time().$_FILES['profile_img']['name'];
        $config['file_name'] = $profileimage;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('profile_img')) 
        {
            $error = array('error' => $this->upload->display_errors()); 
            print_r($error);
            return FALSE;
        } 
        else 
        {
            $data = array('upload_data' => $this->upload->data());
            return $profileimage;
        }
    }
    public function do_upload_signature() 
    {
        $config['upload_path'] = './upload/signature';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '300000';
        $config['max_width'] = '150';
        $config['max_height'] = '200';
        $config['overwrite'] = TRUE;
        $signature = time().$_FILES['sign']['name']; 
        $config['file_name'] = $signature;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('sign')) 
        {
            $error = array('error' => $this->upload->display_errors()); 
            print_r($error);
            return FALSE;
        } 
        else 
        {
            $data = array('upload_data' => $this->upload->data());
            return $signature;
        }
    }
    public function profile_registration()
    {
        $image=$this->do_upload_profile_image();
        // if(!$image)
        // {
        //     $this->session->set_flashdata('imageerr','Failed...Image Size or Format Mismatches!');
        //     redirect('register_final');
        // }
        $signature=$this->do_upload_signature();
        // if(!$signature)
        // {
        //     $this->session->set_flashdata('signatureerr','Failed...Signature Size or Format Mismatches!');
        //     redirect('register_final');
        // }
        if(isset($_POST['agree']) && $_POST['agree'] == 1) {
        $cand_registration=$this->passion_model->profile_registration($image,$signature);
            if($cand_registration)
            {
                //echo "hhh";exit;
                $this->session->set_flashdata('profileregistration','You Are Registered Successfully');
                redirect('instruction');
            }
            else
            {
                $this->session->set_flashdata('profileregistrationerror','Failed.... Please try again...');
                redirect('register_final');
            }
        }
        else{
            $this->session->set_flashdata('declarationerror','Please check the declaration!!');
            redirect('register_final');
        }
    }

    public function getState()
    {
        $this->passion_model->getState();
    }
    public function getCity()
    {
        $this->passion_model->getCity();
    }

    public function logout()
	{
	    $this->session->unset_userdata('cand_data');
        $this->session->sess_destroy();
		$this->load->view('login');
	}
}
?>
